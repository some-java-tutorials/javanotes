import textio.TextIO;

import java.util.TreeSet;

/**
 * A command-line program for processing basic set operations on two sets
 * that are provided by the user. No empty sets allowed. Sets can only contain
 * positive integers, separated by commas and optionally spaces.
 *
 * E.g. Input: [1, 2, 3] + [3,  5,  7] Output: [1, 2, 3, 5, 7]
 *
 * Union (+), intersection (*), difference (-) are the operations supported.
 *
 * This program will borrow heavily from SimpleParser1.java as described in
 * <a href="https://math.hws.edu/javanotes/c9/s5.html#recursion.5.3">Chapter 9</a>
 *
 * Like SimpleParser1.java, only expressions with two sets and a single operator will be allowed.
 */
public class SetCalculator {
    private static boolean expressionStart = false; // Expression has been started with a [
    private static boolean commaRequired = false; // If a comma is required before numeric input
    private static TreeSet<Integer> currentSetExpression = new TreeSet<>();
    /**
     * From SimpleParser1.java
     * An object of type ParseError represents a syntax error found in
     * the user's input.
     */
    private static class ParseError extends Exception {
        private char unexpectedChar;
        ParseError(String message) {
            super(message);
        }
        ParseError(String message, char unexpectedChar) {
            super(message);
            this.unexpectedChar = unexpectedChar;
        }

        public char getUnexpectedChar() {
            return unexpectedChar;
        }
    } // end nested class ParseError

    public static void main(String[] args) {
        while(true){
            TextIO.putln("Enter a square-bracketed set of non-zero integers, delimited by commas.");
            TextIO.putln("Press Enter with no input to quit");

            TextIO.skipBlanks();
            if(TextIO.peek() == '\n'){
                break;
            }

            try {
                // User is entering a set expression
                TreeSet<Integer> leftSet = expressionSet(); // This call will handle numeric input
                char op = getOperator();
                TreeSet<Integer> rightSet = expressionSet();

                TreeSet<Integer> solutionSet = doOperation(leftSet, rightSet, op);



                TextIO.skipBlanks();
                if( TextIO.peek() != '\n'){
                    throw new ParseError("Extra data at the end of expression", TextIO.peek());
                }
                TextIO.getln(); // Discard potential additional input

                TextIO.putln();
                TextIO.putln("Final set is " + solutionSet);
            } catch (ParseError e){
                TextIO.putln();
                TextIO.putln("*** Error in input:    " + e.getMessage());
                TextIO.putln("*** Discarding input:  " + TextIO.getln());
            }

            TextIO.putln();
            TextIO.putln();
            System.out.println("Done.");
        }
    }

    /**
     * Reads a set that the user is entering, saving the data into a TreeSet&lt;Integer&gt;. Very similar to
     * SimpleParser1.expressionValue
     * @return TreeSet&lt;Integer&gt; The set created by the input expression
     * @throws ParseError
     */
    private static TreeSet<Integer> expressionSet() throws ParseError{
        TextIO.skipBlanks();

        int currentMember; // The current set member being processed


        if(Character.isDigit(TextIO.peek())){
           // The input to be read is a number.

            // Confirm the expression has started
            if(!expressionStart){
                throw new ParseError("Set expressions must start with an opening square bracket [. Instead found " +
                        TextIO.peek(), TextIO.peek());
            }

            if(!currentSetExpression.isEmpty()){
                // A comma may be required
                if(commaRequired){
                    throw new ParseError("Comma missing before number", TextIO.peek());
                }
            }

            // A comma will be required after reading this number
            commaRequired = true;

            // Confirm the number is non-negative
            // Read the number
            currentMember = TextIO.getInt();

            if(currentMember < 0){
                throw new ParseError("Number must be non-negative. Found " + currentMember);
            }

            currentSetExpression.add(currentMember);
            expressionSet();
        } else if (TextIO.peek() == '['){
            // Begin reading the set
            TextIO.getAnyChar(); // Skip over '['
            expressionStart = true; // Set expression input has started

            // Reset current expression set

            currentSetExpression = new TreeSet<>();

            try{
                currentSetExpression = expressionSet();
            } catch (ParseError e){
                // The only unexpected character that is expected is one that
                // ends this set
                if( TextIO.peek() != ']'){
                    throw new ParseError("Missing ending square bracket");
                } else {
                    commaRequired = false;
                    expressionStart = false;
                    TextIO.getAnyChar(); // Skip over ']'
                }
            }

        } else if (TextIO.peek() == ','){
            if(commaRequired){
                TextIO.getAnyChar(); // Skip over this expected comma
                commaRequired = false;
                expressionSet();
            } else {
                // A comma is not allowed here
                throw new ParseError("Found unexpected comma: ", TextIO.peek());
            }
        }
        else {
            throw new ParseError("Encountered unexpected character, \"" +
                    TextIO.peek() + "\" in input.", TextIO.peek());
        }

        return currentSetExpression;
    }

    private static char getOperator() throws ParseError{
        TextIO.skipBlanks();

        char op = switch(TextIO.peek()) {
            case '+', '-', '*' -> TextIO.peek();
            default -> throw new ParseError("Unexpected value: " + TextIO.peek(), TextIO.peek());
        };
        TextIO.getAnyChar(); // Skip over the operator character

        return op;
    }

    private static TreeSet<Integer> doOperation(TreeSet<Integer> leftSet, TreeSet<Integer> rightSet, char op) throws ParseError{
        switch(op) {
            case '+' -> leftSet.addAll(rightSet);
            case '-' -> leftSet.removeAll(rightSet);
            case '*' -> leftSet.retainAll(rightSet);
            default -> throw new ParseError("Unexpected value: " + TextIO.peek(), TextIO.peek());
        }

        return leftSet;
    }
}
