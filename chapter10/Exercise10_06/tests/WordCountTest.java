import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class WordCountTest {

    @Test
    void readFile() {
        WordCount.ReadFile("sample.txt");
        TreeMap<String, WordCount.WordData> map = WordCount.getWordLineMap();

        assertNotEquals(0, map.size());
    }
}