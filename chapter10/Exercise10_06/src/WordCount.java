
import textio.TextIO;

import java.util.*;

/**
 *  This program will read words from an input file, and count the
 *  number of occurrences of each word.  The word data is written to
 *  an output file twice, once with the words in alphabetical order
 *  and once with the words ordered by number of occurrences.  The
 *  user specifies the input file and the output file.
 *
 *  The program demonstrates several parts of Java's framework for
 *  generic programming:  TreeMap, List sorting, Comparators, etc.
 */
public class WordCount {
    private static final TreeMap<String, WordData> wordLineMap = new TreeMap<>();
    /**
     * Represents the data we need about a word:  the word and
     * the line number on which it has been encountered.
     */
    public static class WordData {
        String word;
        TreeSet<Integer> lines;
        WordData(String w) {
                // Constructor for creating a WordData object when
                // we encounter a new word.
            word = w;
        }
    } // end class WordData


    /**
     * Read the next word from TextIO, if there is one.  First, skip past
     * any non-letters in the input.  If an end-of-file is encountered before 
     * a word is found, return null.  Otherwise, read and return the word.
     * A word is defined as a sequence of letters.  Also, a word can include
     * an apostrophe if the apostrophe is surrounded by letters on each side.
     * @return the next word from TextIO, or null if an end-of-file is encountered
     */
    private static String readNextWord() {
        char ch = TextIO.peek(); // Look at next character in input.
        while (ch != TextIO.EOLN && ! Character.isLetter(ch)) {
            TextIO.getAnyChar();  // Read the character.
            ch = TextIO.peek();   // Look at the next character.
        }
        if (ch == TextIO.EOLN) { // Encountered end-of-line
            TextIO.getAnyChar();
            return null;
        }
        // At this point, we know that the next character is a letter, so read a word.
        String word = "";  // This will be the word that is read.
        while (true) {
            word += TextIO.getAnyChar();  // Append the letter onto word.
            ch = TextIO.peek();  // Look at next character.
            if ( ch == '\'' ) {
                    // The next character is an apostrophe.  Read it, and
                    // if the following character is a letter, add both the
                    // apostrophe and the letter onto the word and continue
                    // reading the word.  If the character after the apostrophe
                    // is not a letter, the word is done, so break out of the loop.
                TextIO.getAnyChar();   // Read the apostrophe.
                ch = TextIO.peek();    // Look at char that follows apostrophe.
                if (Character.isLetter(ch)) {
                    word += "'" + TextIO.getAnyChar();
                    ch = TextIO.peek();  // Look at next char.
                }
                else
                    break;
            }
            if ( ! Character.isLetter(ch) ) {
                    // If the next character is not a letter, the word is
                    // finished, so break out of the loop.
                break;
            }
            // If we haven't broken out of the loop, next char is a letter.
        }
        return word;  // Return the word that has been read.
    }

    /**
     * Add a line reference to the index.
     */
    private static void addReference(String term, int lineNum) {
        WordData references; // The set of page references that we
        //    have so far for the term.
        references = wordLineMap.get(term);
        if (references == null){
            // This is the first reference that we have
            // found for the term.  Make a new set containing
            // the page number and add it to the index, with
            // the term as the key.
            WordData data = new WordData(term);
            data.lines = new TreeSet<>();
            data.lines.add( lineNum );  // lineNum is "autoboxed" to give an Integer!

            wordLineMap.put(term,data);
        }
        else {
            // references is the set of page references
            // that we have found previously for the term.
            // Add the new page number to that set.  This
            // set is already associated to term in the index.
            references.lines.add( lineNum );
        }
    }

    public static TreeMap<String, WordData> getWordLineMap() {
        return wordLineMap;
    }

    public static void ReadFile(String filePath) {
        TextIO.readFile(filePath);
        String currentWord;
        int currentLine = 1;

        while(TextIO.peek() != TextIO.EOF){
            currentWord = readNextWord();
            while(currentWord != null){
                currentWord = currentWord.toLowerCase();
                addReference(currentWord, currentLine);
                currentWord = readNextWord();
            }
            currentLine++;
        }
    }

    public static void WriteFile(String filePath){
        TextIO.writeFile(filePath);

        for(var wordData : wordLineMap.entrySet()){
            WordData data = wordData.getValue();
            TextIO.putln(data.word);

            for(var lineEntry : data.lines){
                TextIO.put('\t');
                TextIO.putln(lineEntry);
            }
            TextIO.putln();
        }
    }
} // end class WordCount


