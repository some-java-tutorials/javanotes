import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PredicatesDemo {
    private static List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5, 6);

    public static void main(String[] args) {
        ArrayList<Integer> aList = new ArrayList<>(intList);
        System.out.println(aList);

        Predicates.remove(aList, i -> i < 4);

        System.out.println(aList);
        System.out.println();

        ArrayList<Integer> bList = new ArrayList<>(intList);

        System.out.println(bList);

        Predicates.retain(bList, i -> i < 4);

        System.out.println(bList);
        System.out.println();

        ArrayList<Integer> cList = new ArrayList<>(intList);

        System.out.println(cList);

        List<Integer> newList = Predicates.collect(cList, i -> i < 4);

        System.out.println(newList);
        System.out.println();

        ArrayList<Integer> dList = new ArrayList<>(intList);

        System.out.println(dList);

        int index = Predicates.find(dList, i -> i > 3);

        System.out.println(index);
        System.out.println();
    }
}
