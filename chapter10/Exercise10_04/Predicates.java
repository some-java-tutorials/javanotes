import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class Predicates {
    public static <T> void remove(Collection<T> coll, Predicate<T> pred){
        ArrayList<T> toBeRemoved = new ArrayList<>();

        for(T item : coll){
            if(pred.test(item)){
                toBeRemoved.add(item);
            }
        }

        coll.removeAll(toBeRemoved);
    }

    public static <T> void retain(Collection<T> coll, Predicate<T> pred){
        ArrayList<T> toBeRemoved = new ArrayList<>();

        for(T item : coll){
            if(Predicate.not(pred).test(item)){
                toBeRemoved.add(item);
            }
        }

        coll.removeAll(toBeRemoved);
    }

    public static <T> List<T> collect(Collection<T> coll, Predicate<T> pred){
        List<T> toBeReturned = new ArrayList<>();

        for(T item : coll){
            if(pred.test(item)){
                toBeReturned.add(item);
            }
        }

        return toBeReturned;
    }

    public static <T> int find(ArrayList<T> list, Predicate<T> pred){
        int foundIndex = -1;
        for(int i = 0; i < list.size(); i++){
            if(pred.test(list.get(i))){
                foundIndex = i;
                break;
            }
        }

        return foundIndex;
    }
}
