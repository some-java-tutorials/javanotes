import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;

public class ScoreStream {
    public static void main(String[] args) {
        // Count
        long numOfScores = Arrays.stream(scoreData).count();

        System.out.println("Number of scores: " + numOfScores);

        // Average
        OptionalDouble average = Arrays.stream(scoreData).mapToInt(ScoreInfo::score).average();

        System.out.println("Average score: " + average.getAsDouble());

        // Number of students with As
        long numOfAs = Arrays.stream(scoreData).filter(s -> s.score >= 90 ).count();

        System.out.println("Number of students with As: " + numOfAs);
        System.out.println();

        List<String> lessThan70 = Arrays.stream(scoreData).filter(s -> s.score < 70).map(s -> s.firstName + " " + s.lastName).toList();
        System.out.println("Students who scored less than 70:");
        System.out.println(lessThan70);
        System.out.println();

        System.out.println("Students sorted by last name: ");
        Arrays.stream(scoreData)
                .sorted(Comparator.comparing(a -> a.lastName))
                .forEach(s -> System.out.println("Name: " + s.firstName + " " + s.lastName + ". Score: " + s.score));
        System.out.println();

        System.out.println("Students sorted by score: ");
        Arrays.stream(scoreData)
                .sorted(Comparator.comparingInt(a -> a.score))
                .forEach(s -> System.out.println("Name: " + s.firstName + " " + s.lastName + ". Score: " + s.score));

    }
    /**
     * Data for one student about a score on a test.
     */
    private record ScoreInfo(
            String lastName,
            String firstName,
            int score
    ) { }

    private static final ScoreInfo[] scoreData = new ScoreInfo[] {
            new ScoreInfo("Smith","John",70),
            new ScoreInfo("Doe","Mary",85),
            new ScoreInfo("Page","Alice",82),
            new ScoreInfo("Cooper","Jill",97),
            new ScoreInfo("Flintstone","Fred",66),
            new ScoreInfo("Rubble","Barney",80),
            new ScoreInfo("Smith","Judy",48),
            new ScoreInfo("Dean","James",90),
            new ScoreInfo("Russ","Joe",55),
            new ScoreInfo("Wolfe","Bill",73),
            new ScoreInfo("Dart","Mary",54),
            new ScoreInfo("Rogers","Chris",78),
            new ScoreInfo("Toole","Pat",51),
            new ScoreInfo("Khan","Omar",93),
            new ScoreInfo("Smith","Ann",95)
    };


}
