import textio.TextIO;

import java.util.TreeMap;

public class PhoneDirectory {
    public static TreeMap<String, String> directory = new TreeMap<>();
    public static String phoneNumber = "";
    public static String name = "";
    public static void main(String[] args) {
        while(userWantsToContinue()){
            TextIO.putln("Enter a name for the phone number");

            name = TextIO.getln();
            if(name.isEmpty()){
                TextIO.putln("No input provided");
                continue;
            }

            directory.put(phoneNumber, name);

            TextIO.putln("Current directory content: " + directory);
        }
    }

    public static boolean userWantsToContinue() {
        boolean userWillContinue = true;

        TextIO.put("Enter a phone number for the directory ('q' to quit): ");
        phoneNumber = TextIO.getln();

        if(phoneNumber.isEmpty()){
            TextIO.putln("No input provided");
            return true;
        }

        if(phoneNumber.equalsIgnoreCase("q")){
            userWillContinue = false;
        }

        return userWillContinue;
    }

}