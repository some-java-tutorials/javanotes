/**
 * A very simple implementation of a hash map with String keys and values.
 *
 */
public class SimpleHashMap {
    /**
     * The size of the array that contains all the hash map data. For this exercise, it will be very small
     * and it will not increase.
     */
    public static final int ARRAY_SIZE = 10;
    /**
     * An array of linked lists that are storing String values that correspond with String keys. The string
     * keys are converted to an array index with stringHashToIndex(string.hashCode())
     */
    private Node[] dataArray = new Node[10];

    /**
     *
     * @param key Converted to an array index
     * @param value Will be saved in a linked list node
     */
   public void put(String key, String value){
       int keyHashCode = key.hashCode();
       int dataArrayIndex = stringHashToIndex(keyHashCode);

       if(dataArray[dataArrayIndex] == null){
           dataArray[dataArrayIndex] = new Node(value, keyHashCode);
       } else {
           Node currentNode = dataArray[dataArrayIndex];

           if(findValueByKeyHashCode(currentNode, keyHashCode) != null) return; // No duplicate entries

           while(currentNode.next != null){
               currentNode = currentNode.next;
               if(findValueByKeyHashCode(currentNode, keyHashCode) != null) return; // No duplicate entries
           }

           currentNode.next = new Node(value, keyHashCode);
       }
   }

    /**
     * Get the String value for the given String key
     * @param key
     * @return
     */
   public String get(String key) throws IndexOutOfBoundsException{
       // Find the array index for this String key

       int keyHashCode = key.hashCode();
       int dataArrayIndex = stringHashToIndex(keyHashCode);

       if(dataArray[dataArrayIndex] == null){
           throw new IndexOutOfBoundsException("Could not find " + key + " in the hash map");
       }

       Node currentNode = dataArray[dataArrayIndex];

       while(findValueByKeyHashCode(currentNode, keyHashCode) == null){
           currentNode = currentNode.next;

           if (currentNode == null) {
               throw new IndexOutOfBoundsException("Could not find " + key + " in the hash map");
           }
       }

       return currentNode.value;
   }

    /**
     * Remove an entry by key
     * @param key
     */
   public void remove(String key) throws IndexOutOfBoundsException{
       int keyHashCode = key.hashCode();

       Node prevNode = null;
       Node currentNode = dataArray[stringHashToIndex(keyHashCode)];

       if(currentNode == null){
           throw new IndexOutOfBoundsException("Could not find " + key + " in the hash map");
       }

       while(currentNode.keyHash != keyHashCode){
           prevNode = currentNode;
           currentNode = currentNode.next;
       }

       if(prevNode == null){
           dataArray[stringHashToIndex(keyHashCode)] = null;
       } else {
           prevNode.next = currentNode.next;
       }
   }

   public boolean containsKey(String key){
       boolean valueFound = false;
       int keyHashCode = key.hashCode();

       for(int i = 0; i < dataArray.length; i++){
           if (dataArray[i] != null) {
               Node runner = dataArray[i];

               while(runner != null){
                   if(findValueByKeyHashCode(runner, keyHashCode) != null){
                       valueFound = true;
                       break;
                   }

                   runner = runner.next;
               }
           }
       }

       return valueFound;
   }

   public int size(){
       int count = 0;
       for(int i = 0; i < dataArray.length; i++){
           Node runner = dataArray[i];

           while(runner != null){
               count++;
               runner = runner.next;
           }
       }

       return count;
   }

   private String findValueByKeyHashCode(Node node, int keyHashCode){
       if(node.keyHash == keyHashCode){
           return node.value;
       }

       return null;
   }

    /**
     * A linked list node
     */
   private class Node{
        /**
         * The link to the next node, or null
         */
       private Node next;
        /**
         * The value being stored in this node
         */
       private String value;
        /**
         * The original string.hashCode for the key used to store this value
         */
       private int keyHash;

       public Node(String value, int keyHash){
           this.value = value;
           this.keyHash = keyHash;
       }
   }

    /**
     * Convert the result of string.hashCode() to an index to be
     * used for the dataArray. This index will always be non-negative.
     * @param stringHash A possibly negative and possibly large number
     * @return
     */
   private int stringHashToIndex(int stringHash){
       return Math.abs(stringHash) % ARRAY_SIZE;
   }
}
