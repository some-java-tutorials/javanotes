public class SimpleHashMapDemo {
    public static void main(String[] args) {
        // Test storage of strings
        SimpleHashMap hashMap = new SimpleHashMap();

        hashMap.put("do", "not");
        hashMap.put("stop", "thinking");
        hashMap.put("about", "tomorrow");

        System.out.println(hashMap.get("do"));
        System.out.println(hashMap.get("stop"));
        System.out.println(hashMap.get("about"));

        hashMap.remove("stop");

        try {
            System.out.println(hashMap.get("stop"));
        } catch (IndexOutOfBoundsException e){
            System.out.println("'Stop' no longer in hashMap");
        }

        System.out.println(hashMap.get("do"));

        System.out.println(hashMap.get("about"));

        hashMap.put("riddle", "me this");
        hashMap.put("riddle", "me that");

        if(!hashMap.get("riddle").equals("me this")){
            System.err.println("hashMap.get(\"riddle\") has value " + hashMap.get("riddle"));
        }

        if (hashMap.containsKey("about")) {
            System.out.println("'about' is still present " + hashMap.get("about"));
        }

        if (!hashMap.containsKey("stop")) {
            System.out.println("'stop' is still not present ");
        }

        System.out.println("hashMap size: " + hashMap.size());
    }
}
