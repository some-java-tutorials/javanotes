public class Roll {
    public static void main(String[] args) {
        PairOfDice dice = new PairOfDice();

        int diceTotal = dice.getDie1() + dice.getDie2();

        int rolls = 1;
        System.out.println(dice);

        while(diceTotal != 2){
            dice.roll();
            System.out.println(dice);
            diceTotal = dice.getDie1() + dice.getDie2();
            rolls++;
        }

        System.out.println(String.format("Numer of roles: %d", rolls));
    }
}
