import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairOfDiceTest {

    @Test
    void can_show_roll() {
        PairOfDice dice = new PairOfDice();
        assertTrue(dice.toString().contains("Die 1: " + dice.getDie1()));
        assertTrue(dice.toString().contains("Die 2: " + dice.getDie2()));
    }
}