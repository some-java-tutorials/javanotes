import textio.TextIO;

public class ComputeStatistics {
    public static void main(String[] args) {
        StatCalc  calc;   // Object to be used to process the data.
        calc = new StatCalc();

        System.out.println("Enter a number (0 to quit): ");
        double nextNumber = TextIO.getlnDouble();

        while(nextNumber != 0){                  
            calc.enter(nextNumber);
            System.out.println("Enter a number (0 to quit): ");
            nextNumber = TextIO.getlnDouble();
        }

        System.out.println("Min: " + calc.getMin());
        System.out.println("Max: " + calc.getMax());
        System.out.println("Sum: " + calc.getSum());
        System.out.println("Mean: " + calc.getMean());
        System.out.println("Standard Deviation: " + calc.getStandardDeviation());
        
    }
}
