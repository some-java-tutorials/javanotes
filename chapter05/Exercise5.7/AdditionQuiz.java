import textio.TextIO;
import java.util.Random;

public class AdditionQuiz {
    private static final int QUIZ_LENGTH = 10;
    private static final int OPERAND_MAX = 50;
    private static IntQuestion[] questions = new IntQuestion[QUIZ_LENGTH];
    private static int[] answers = new int[10];

    public static class AdditionQuestion implements IntQuestion{

        private int a, b;  // The numbers in the problem.
    
        public AdditionQuestion() { // constructor
            Random random = new Random();
            a = random.nextInt(OPERAND_MAX) + 1;
            b = random.nextInt(OPERAND_MAX);
        }
    
        public String getQuestion() {
            return "What is " + a + " + " + b + " ?";
        }
    
        public int getCorrectAnswer() {
            return a + b;
        }
    
    }

    public static class SubtractionQuestion implements IntQuestion{

        private int a, b;  // The numbers in the problem.
    
        public SubtractionQuestion() { // constructor
            Random random = new Random();
            a = random.nextInt(OPERAND_MAX) + 1;
            b = random.nextInt(OPERAND_MAX);

            if(a < b){
                int temp = a;
                a = b;
                b = temp;
            }
        }
    
        public String getQuestion() {
            return "What is " + a + " - " + b + " ?";
        }
    
        public int getCorrectAnswer() {
            return a - b;
        }
    
    }

    public static interface IntQuestion {
        public String getQuestion();
        public int getCorrectAnswer();
    }

    static void doQuiz(){
        boolean continueQuiz = false;
        int score = 0;
        int correctAnswer = 0;

        do{
            for(int i = 0; i < questions.length; i++){
                System.out.printf(questions[i].getQuestion() + " ");
                answers[i] = TextIO.getlnInt();

                correctAnswer = questions[i].getCorrectAnswer();
                if(answers[i] == correctAnswer){
                    System.out.println("Correct!");
                    score++;
                } else {
                    System.out.printf("The answer is: %d%n", correctAnswer);
                }
            }
            System.out.printf("Score: %d%n", score);
            System.out.println("Do quiz again?");
            continueQuiz = TextIO.getlnBoolean();
        }while(continueQuiz);   
    }
    public static void main(String[] args) {
        for(int i = 0; i < 8; i++){
            if(Math.random() < 0.5){
                questions[i] = new AdditionQuestion();
            } else {
                questions[i] = new SubtractionQuestion();
            }
        }
        IntQuestion hitchhiker = new IntQuestion() {
            public String getQuestion() {
                return "What is the answer to the ultimate question " +
                         " of life, the universe, and everything?";
            }
            public int getCorrectAnswer() {
                return 42;
            }
        };

        IntQuestion mrshow = new IntQuestion() {
            public String getQuestion() {
                return "What is the highest number?";
            }
            public int getCorrectAnswer() {
                return 24;
            }
        };

        questions[8] = hitchhiker;
        questions[9] = mrshow;

        doQuiz();
    }
}
