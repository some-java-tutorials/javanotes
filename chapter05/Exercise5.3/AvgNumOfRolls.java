public class AvgNumOfRolls {
    public static void main(String[] args) {
        final int TOTAL_START = 2;
        final int TOTAL_END = 12;
        final int NUM_OF_TRIALS = 10000;

        StatCalc statCalc = new StatCalc();

        System.out.println("Total On Dice     Avg of Rolls    Max Rolls    Standard Deviation");
        System.out.println("-------------     -------------   ----------   -------------");
        for(int i = TOTAL_START; i <= TOTAL_END; i++){
            double avg, stdDev;
            int max;
            for(int j = 1; j <= NUM_OF_TRIALS; j++){
                statCalc.enter(RollDice.guessRoll(i));
            }
            avg = statCalc.getMean();
            max = (int) statCalc.getMax();
            stdDev = statCalc.getStandardDeviation();

            System.out.printf("%13d%18.4f%13d%16.4f%n", i, avg, max, stdDev);
        }
    }
}
