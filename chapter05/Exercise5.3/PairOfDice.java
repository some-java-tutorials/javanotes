public class PairOfDice {

    private int die1 = (int)(Math.random()*6) + 1;
    private int die2 = (int)(Math.random()*6) + 1;

    public void roll() {
            // Roll the dice by setting each of the dice to be
            // a random number between 1 and 6.
         die1 = (int)(Math.random()*6) + 1;
         die2 = (int)(Math.random()*6) + 1;
    }

    public int getDie1() {
        return die1;
    }

    public int getDie2() {
        return die2;
    }
    
    public String toString(){
        return String.format("Die 1: %d, Die 2: %d", die1, die2);
    }
}