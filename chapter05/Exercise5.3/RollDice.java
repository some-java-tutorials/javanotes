public class RollDice{
    public static PairOfDice dice = new PairOfDice();
    public static void main(String[] args) {
        int numOfRoles;
        System.out.println("Rolling for snake eyes...");
        numOfRoles = guessRoll(2);
        System.out.printf("You got snake eyes after %d rolls!%n", numOfRoles);
    }
    /**
     * Roll the dice
     * @return the total after rolling dice
     */
    public static int roll(){        
        dice.roll();
        return dice.getDie1() + dice.getDie2();
    }
    /**
     * 
     * @param guess The amount the user is rolling for
     * @return The number of times the dice was rolled before the guess was correct
     */
    public static int guessRoll(int guess){
        int numOfRoles = 0;
        while(roll() != guess){
            numOfRoles++;
        }
        return numOfRoles;
    }
}