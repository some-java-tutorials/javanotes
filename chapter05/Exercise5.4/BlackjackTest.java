import textio.TextIO;

public class BlackjackTest {
    public static void main(String[] args) {
        System.out.println("Deal Blackjack Hand? (y/n)");

        String input = TextIO.getln();

        while(input.equalsIgnoreCase("y")){
            doBlackjack();

            System.out.println("Deal Blackjack Hand? (y/n)");

            input = TextIO.getlnString();
        }
    }

    public static void doBlackjack(){
        Deck deck = new Deck();
        BlackjackHand hand = new BlackjackHand();

        int handSize = (int)(Math.random() * 5) + 2;

        deck.shuffle();

        System.out.println("Hand size: " + handSize);
        for(int i = 0; i < handSize; i++){
            hand.addCard(deck.dealCard());
            System.out.println(hand.getCard(i));
        }

        System.out.println("Blackjack value: " + hand.getBlackjackValue());
    }
}
