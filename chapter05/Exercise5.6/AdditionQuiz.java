import textio.TextIO;

public class AdditionQuiz {
    private static final int QUIZ_LENGTH = 10;
    private static final int OPERAND_MAX = 50;
    static int[] answers = new int[QUIZ_LENGTH];

    public static class AdditionQuestion {

        private int a, b;  // The numbers in the problem.
    
        public AdditionQuestion() { // constructor
            a = (int)(Math.random() * 50 + 1);
            b = (int)(Math.random() * 50);
        }
    
        public String getQuestion() {
            return "What is " + a + " + " + b + " ?";
        }
    
        public int getCorrectAnswer() {
            return a + b;
        }
    
    }

    static void doQuiz(){
        boolean continueQuiz = false;
        int score = 0;
        int correctAnswer = 0;

        do{
            for(int i = 0; i < answers.length; i++){
                AdditionQuestion question = new AdditionQuestion();

                System.out.printf(question.getQuestion());
                answers[i] = TextIO.getlnInt();

                correctAnswer = question.getCorrectAnswer();
                if(answers[i] == correctAnswer){
                    System.out.println("Correct!");
                    score++;
                } else {
                    System.out.printf("The answer is: %d%n", correctAnswer);
                }
            }
            System.out.printf("Score: %d%n", score);
            System.out.println("Do quiz again?");
            continueQuiz = TextIO.getlnBoolean();
        }while(continueQuiz);   
    }
    public static void main(String[] args) {
        doQuiz();
    }
}
