import textio.TextIO;

public class Game {
    private static Deck deck;
    private static BlackjackHand dealerHand;
    private static BlackjackHand playerHand;

    private static char playerContinue;
    private static boolean gameOver;

    public static void main(String[] args) {
        boolean restart = true;

        while(restart){
            if(playGame()){
                System.out.println("You win!");
            } else {
                System.out.println("Dealer wins");
            }

            System.out.println("Play again? (y/n)");
            char playAgain = TextIO.getlnChar();
            restart = Character.toString(playAgain).equalsIgnoreCase("y");

            System.out.println();
        }
    }

    public static boolean playGame(){
        boolean playerWins = false;
        gameOver = false;
        playerContinue = '\0';

        deck = new Deck();
        deck.shuffle();

        dealerHand = new BlackjackHand();
        playerHand = new BlackjackHand();

        // Deal two cards to each player's hand
        initialDeal(dealerHand, true);
        initialDeal(playerHand, false);

        if(dealerHand.getBlackjackValue() == 21){
            System.out.println("Dealer's score is 21.");
            gameOver = true;
            return playerWins;
        } else if(playerHand.getBlackjackValue() == 21){
            gameOver = true;
            playerWins = true;
            return playerWins;
        }

        while(!(playerContinue == 'h' || playerContinue == 's')){
            playerWins = hit(playerHand, false);
        }

        if(playerHand.getBlackjackValue() > 21){
            System.out.println("You busted.");
            gameOver = true;
            playerWins = false;
            return playerWins;
        }

        if(playerContinue == 's' && dealerHand.getBlackjackValue() >= playerHand.getBlackjackValue()){
            System.out.println("Dealer's score is greater.");
            gameOver = true;
            playerWins = false;
            return playerWins;
        }

        while(!gameOver && playerContinue != 's' && ((playerContinue == 'h' && playerHand.getBlackjackValue() < 21) || !playerWins)){
            if(playerContinue == 'h'){
                playerWins = hit(playerHand, false);
            }
        }

        if(playerHand.getBlackjackValue() > 21){
            System.out.println("You busted.");
            gameOver = true;
            playerWins = false;
            return playerWins;
        }

        if(playerWins){
            return playerWins;
        }

        // Player stays. Dealer's turn.
        System.out.println("Dealer's turn...");
        while(dealerHand.getBlackjackValue() <= 16){
            playerWins = !hit(dealerHand, true);
        }

        if(dealerHand.getBlackjackValue() > 21){
            System.out.println("Dealer busted.");
            gameOver = true;
            playerWins = false;
            return playerWins;
        }

        if(dealerHand.getBlackjackValue() >= playerHand.getBlackjackValue()){
            System.out.println("Dealer's score is greater than or equal to yours.");
            gameOver = true;
            playerWins = false;
            return playerWins;
        }

        if(dealerHand.getBlackjackValue() < playerHand.getBlackjackValue()){
            System.out.println("Your score is greater");
            gameOver = true;
            playerWins = true;
            return playerWins;
        }
        return playerWins;
    }

    public static void initialDeal(BlackjackHand hand, boolean isDealer){
        if(isDealer){
            System.out.println("Dealer:");
        } else {
            System.out.println("You:");
        }
        
        for(int i = 0; i < 2; i++){
            hand.addCard(deck.dealCard());
            if(!isDealer || i == 0){
                System.out.println("  " + hand.getCard(i));
            } else {
                System.out.println("  xxxxxxxxxxxx");
            }
        }

        // Show Score
        System.out.println("(" + hand.getBlackjackValue() + ")");

    }

    /**
     * Hit the player 
     * @param hand
     * @param isDealer
     * @return True if player lost
     */
    private static boolean hit(BlackjackHand hand, boolean isDealer){
        if(!isDealer && hand.getBlackjackValue() < 21){
            System.out.println("Hit or Stay? (h/s)");
            playerContinue = TextIO.getln().toLowerCase().charAt(0);

            if(playerContinue == 's'){
                return false;
            }
        }

        if(hand.getBlackjackValue() == 21){
            gameOver = true;
            return true;
        }

        if(hand.getBlackjackValue() > 21){
            gameOver = true;
            return false;
        }

        hand.addCard(deck.dealCard());

        System.out.println();
        if(isDealer){

            System.out.println("Dealer hits...");
            System.out.println("Dealer:");
        } else {
            System.out.println("You:");
        }
        for(int i = 0; i < hand.getCardCount(); i++){            

            System.out.println("  " + hand.getCard(i));
        }
        // Show Score
        System.out.println("(" + hand.getBlackjackValue() + ")");

        return hand.getBlackjackValue() == 21;
    }
}