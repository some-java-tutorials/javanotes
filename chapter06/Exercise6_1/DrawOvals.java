import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class DrawOvals extends Application {
    private Canvas canvas;      // The canvas where everything is drawn.
    private GraphicsContext g;
    private final int width = 640, height = 480; // The size of the canvas
    private boolean dragging;
    private double prevX, prevY;

    private double startX, startY;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Start method creates the window content and configures event listening.
     */
    public void start(Stage stage) {
        canvas = new Canvas(width,height);
        canvas.setOnMousePressed(e -> mousePressed(e));
        canvas.setOnMouseDragged(e -> mouseDragged(e));
        canvas.setOnMouseReleased(e -> mouseReleased(e));

        Pane root = new Pane(canvas);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Drag to draw some shapes");
        stage.setResizable(false);

        stage.show();
    }

    private void draw(MouseEvent evt){
        g = canvas.getGraphicsContext2D();

        if(evt.isPrimaryButtonDown()){
            if (evt.isShiftDown()) {
                g.setFill( Color.BLUE );
                g.fillOval( evt.getX() - 30, evt.getY() - 15, 60, 30 );
            }
            else {
                g.setFill( Color.RED );
                g.fillRect( evt.getX() - 30, evt.getY() - 15, 60, 30 );
            }
        }
    }

    private void mousePressed(MouseEvent evt){
        if (dragging) {
            // The user pressed a second mouse button before releasing the first.
            // Ignore the second button press.
          return;
      }

      if(evt.isSecondaryButtonDown()){
        clear();
      }

      dragging = true;
      startX = evt.getX();  // Remember starting position.
      startY = evt.getY();
      prevX = startX;       // Remember most recent coords.
      prevY = startY;

      draw(evt);
    }

    public void mouseDragged(MouseEvent evt) {
        if ( dragging == false )  {
            return;
        }

        double x = evt.getX(); // Current position of Mouse.
        double y = evt.getY();

        //double distance = Math.sqrt((x - prevX) * (x - prevX) + (y - prevY) * (y - prevY));

        if( Math.abs(x - prevX) < 15 && Math.abs(y - prevY) < 15){
            return;
        }
        draw(evt);
        prevX = x;
        prevY = y;
    }

    public void mouseReleased(MouseEvent evt) {
        if ( dragging == false ){
            return;             
        }
        dragging = false;  // We are done dragging.
    }

    public void clear(){
        g.setFill(Color.WHITE);
        g.fillRect(0,0,width,height);
    }
}