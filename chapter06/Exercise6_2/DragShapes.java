import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class DragShapes extends Application {
    private Canvas canvas;
    private GraphicsContext g;
    private Color[] colors = new Color[] { Color.RED, Color.BLUE };
    private DraggableShape[] shapes;
    private final int width = 640,
     height = 480,
     shapeWidth = 50,
     shapeHeight = 50;
    private boolean draggingBlue, draggingRed;
    private double mousePrevX, mousePrevY;

    private double mouseCurrentX, mouseCurrentY;;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        canvas = new Canvas(width,height);

        Pane root = new Pane(canvas);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Drag to drag some shapes");
        stage.setResizable(false);

        g = canvas.getGraphicsContext2D();

        initializeStage();

        scene.setOnKeyPressed(this::keyPressed);

        stage.show();
    }

    public Canvas getCanvas(){
        return canvas;
    }

    private void initializeStage(){
        clear();            
    }

    private void drawBackground(){
        g.setFill(Color.WHITE);
        g.fillRect(0,0,width,height);
    }

    private void clear(){
        drawBackground();

        double initialX, initialY;
        shapes = new DraggableShape[colors.length];
        for(int i = 0; i < colors.length; i++){
            initialX = ((2 * i + 1) * canvas.getWidth()) / ( 2 * colors.length);
            initialY = (double) height / 2;
            shapes[i] = new DraggableShape(initialX, initialY, shapeWidth, shapeHeight, colors[i]);
            shapes[i].setApplication(this);
            shapes[i].draw();
            shapes[i].setupMouseHandlers();
        }
    }

    private void keyPressed(KeyEvent evt){
        if(evt.getCode() == KeyCode.ESCAPE){
            clear();
        }
    }

    public void redraw(){
        drawBackground();

        for(int i = 0; i < colors.length; i++){
            if(!shapes[i].isBeingDragging()){
                shapes[i].draw();
            }
        }
    }
}
