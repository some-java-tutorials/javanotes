import javafx.scene.paint.Color;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.application.Application;

public class Shape {
    protected double x, y, w, h; 
    private Color color;
    protected DragShapes app;
    protected Canvas canvas;
    GraphicsContext g;

    public Shape(double x, double y, double w, double h, Color color){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.color = color;
    }

    public void setApplication(DragShapes app){
        this.app = app;
        this.canvas = app.getCanvas();
        g = getGraphicsContext();
    }

    public Canvas getCanvas(){
        return app.getCanvas();
    }

    public GraphicsContext getGraphicsContext(){
        return canvas.getGraphicsContext2D();
    }

    public void draw(){
        g.setFill(color);
        g.fillRect(x, y, w, h);
        g.strokeRect(x, y, w, h);
    }

    public void clear(){
        g.clearRect(x, y, w + 5, h + 5);
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double getWidth(){
        return w;
    }

    public double getHeight(){
        return h;
    }

    public boolean isInArea(double x, double y){
        boolean withinWidth = x >= this.x && x <= (this.x + w);
        boolean withinHeight = y >= this.y && y <= (this.y + h);

        return withinWidth && withinHeight;
    }
}
