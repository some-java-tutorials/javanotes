import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;

public class DrawPolygon extends Application {
    private Canvas canvas;
    private GraphicsContext g;
    private final int CANVAS_WIDTH = 700;
    private final int CANVAS_HEIGHT = 500;

    private boolean firstClick = true;

    private double firstClickedX;
    private double firstClickedY;

    private double lastClickedX;
    private double lastClickedY;

    private double currentClickedX;
    private double currentClickedY;

    private ArrayList<Double> polygonPointsX = new ArrayList<>();
    private ArrayList<Double> polygonPointsY = new ArrayList<>();

    private boolean isNearFirstClick(){
        return Math.abs(currentClickedX - firstClickedX) <= 10
                && Math.abs(currentClickedY - firstClickedY) <= 10;
    }

    private void completePolygon(){
        double[] polygonPointsXArray = new double[polygonPointsX.size()];
        double[] polygonPointsYArray = new double[polygonPointsY.size()];

        for(int i = 0; i < polygonPointsX.size(); i++){
            polygonPointsXArray[i] = polygonPointsX.get(i);
        }
        for(int i = 0; i < polygonPointsY.size(); i++){
            polygonPointsYArray[i] = polygonPointsY.get(i);
        }

        g.setStroke(Color.BLACK);
        g.strokePolygon(polygonPointsXArray, polygonPointsYArray, polygonPointsX.size());
        g.setFill(Color.RED);
        g.fillPolygon(polygonPointsXArray, polygonPointsYArray, polygonPointsX.size());

        // After the polygon is filled, reset the drawing experience
        firstClick = true;
        polygonPointsX = new ArrayList<>();
        polygonPointsY = new ArrayList<>();
    }

    private void clearCanvas(){
        g.setFill(Color.WHITE);
        g.setStroke(Color.BLACK);
        g.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    }

    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage){
        // Create the canvas
        canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        initializeCanvas();

        // Use a BorderPane layout
        BorderPane root = new BorderPane(canvas);

        // Handle click events
        canvas.setOnMouseClicked(this::clickHandler);

        // Set up the scene and stage
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Draw a polygon");
        stage.setResizable(false);

        // Show the stage
        stage.show();
    }

    public void initializeCanvas() {
        g = canvas.getGraphicsContext2D();

        clearCanvas();
    }


    public void clickHandler(MouseEvent e){
        // Store the current click coordinates
        currentClickedX = e.getX();
        currentClickedY = e.getY();

        if(firstClick){
            clearCanvas();

            // On the first click, the current and
            // previous points are the same
            lastClickedX = currentClickedX;
            lastClickedY = currentClickedY;

            // Remember the first place that was clicked
            firstClickedX = currentClickedX;
            firstClickedY = currentClickedY;

            polygonPointsX.add(currentClickedX);
            polygonPointsY.add(currentClickedY);

            // Subsequent clicks are expected
            firstClick = false;

            return;
        }

        if(isNearFirstClick()){
            // Draw a line as the user clicks, snap the last point to the first point
            g.strokeLine(lastClickedX, lastClickedY, firstClickedX, firstClickedY);
            completePolygon();
            return;
        } else {
            // Draw a line as the user clicks
            g.strokeLine(lastClickedX, lastClickedY, currentClickedX, currentClickedY);
            // Add the clicked coordinates to the polygon being built
            polygonPointsX.add(currentClickedX);
            polygonPointsY.add(currentClickedY);
        }

        lastClickedX = currentClickedX;
        lastClickedY = currentClickedY;
    }
}
