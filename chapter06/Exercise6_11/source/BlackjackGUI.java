import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class BlackjackGUI extends Application {
    private GraphicsContext g;
    private Canvas board;
    /**
     * Images of the cards
     */
    private Image cardImages;
    /**
     * Whether a game is in progress
     */

    private Button hitMeButton;
    private Button stayMeButton;
    private Button newGameMeButton;

    private Label gameMessageLabel;
    private TextField betTextField;
    private Label playerPotLabel;

    private boolean playing = false;

    /**
     * Whether the endgame state has been reached. This is when the player
     * stays and the dealer starts to draw.
     */
    private boolean endgame = false;

    /**
     * A message explaining why the game is over
     */
    private String gameEndReason = "";

    private Deck deck;

    private BlackjackPlayer[] blackjackPlayers = new BlackjackPlayer[2];

    private final int CARD_DISPLAY_WIDTH = 99;
    private final int CARD_IMAGE_WIDTH = 79;
    private final int CARD_HEIGHT = 123;
    private final int CARD_HORIZONTAL_BORDER = 20;
    private final int CARD_VERTICAL_BORDER = 10;
    private final int CARDS_PER_ROW = 5;
    private final int NUM_OF_MESSAGES = 2;
    private final int MESSAGE_HEIGHT = 40;

    private final String DEALER_LABEL = "Dealer's Cards:";
    private final String PLAYER_LABEL = "Your Cards:";
    private final String GAME_SCORE_LABEL = "You have %d.";
    private final String GAME_CHOICE_LABEL = "Hit or Stay?";


    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage){
        cardImages = new Image("cards.png");

        /**
         * Space for:
         *  - Five cards horizontally
         *  - Two cards and three text components vertically
         */
        board = new Canvas(
                CARDS_PER_ROW * CARD_DISPLAY_WIDTH + CARD_HORIZONTAL_BORDER,
                (2 * CARD_HEIGHT) + (2 * CARD_VERTICAL_BORDER) + (NUM_OF_MESSAGES * MESSAGE_HEIGHT)
        );
        g = board.getGraphicsContext2D();

        gameMessageLabel = new Label("Welcome to Blackjack");
        gameMessageLabel.setId("gameMessageLabel");
        gameMessageLabel.setTextFill( Color.rgb(220,255,220) );
        gameMessageLabel.setFont( Font.font(16) );

        // Set up the players
        blackjackPlayers[0] = new BlackjackPlayer(true);
        blackjackPlayers[1] = new BlackjackPlayer();

        playerPotLabel = new Label("You have $" + blackjackPlayers[1].getPlayerPot());
        playerPotLabel.setTextFill(Color.YELLOW);
        playerPotLabel.setFont( Font.font(16) );
        playerPotLabel.setId("playerPotLabel");

        VBox mainBox = new VBox(board, playerPotLabel, gameMessageLabel);
        mainBox.setId("mainBox");


        // Create buttons and button bar
        hitMeButton = new Button("Hit Me");
        stayMeButton = new Button("Stay Me");
        newGameMeButton = new Button("New Game Me");

        hitMeButton.setOnAction(this::hitButtonHandler);
        stayMeButton.setOnAction(this::stayButtonHandler);
        newGameMeButton.setOnAction(this::newGameButtonHandler);

        // Disable when the program first starts
        hitMeButton.setDisable(true);
        stayMeButton.setDisable(true);

        Label betLabel = new Label("Your bet:");
        betTextField = new TextField(String.valueOf(blackjackPlayers[1].getPlayerBet()));
        betTextField.setPrefColumnCount(5);
        betTextField.addEventFilter(KeyEvent.KEY_TYPED, this::filterBetValue);
        betTextField.addEventHandler(InputEvent.ANY, this::cleanBetValue);

        HBox buttonBar = new HBox(hitMeButton, stayMeButton, newGameMeButton, betLabel, betTextField);
        buttonBar.setId("buttonBar");
        buttonBar.setSpacing(5);

        BorderPane root = new BorderPane();
        root.setCenter(mainBox);
        root.setBottom(buttonBar);

        Scene scene = new Scene(root);

        scene.getStylesheets().add("css/style.css");

        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Blackjack");

        stage.show();
    }

    private void filterBetValue(KeyEvent e){
        if(!Character.isDigit(e.getCharacter().charAt(0))){
            e.consume();
        }
    }

    private void cleanBetValue(InputEvent e){
        if(betTextField.getText().matches(".*\\D+.*")){
            betTextField.setText(betTextField.getText().replaceAll("\\D+", ""));
        }

        int bet = 0;
        try {
            bet = Math.max(0, Integer.parseInt(betTextField.getText()));
        } catch(NumberFormatException ignored){
        }

        blackjackPlayers[1].setPlayerBet(bet);
    }

    private void startNewGame(){
        if(playing){
            // A current game is already in progress
            return;
        }

        endgame = false;
        newGameMeButton.setDisable(true);
        hitMeButton.setDisable(false);
        stayMeButton.setDisable(false);
        betTextField.setDisable(true);
        gameEndReason = "";

        deck = new Deck();
        deck.shuffle();

        for(int i = 0; i < blackjackPlayers.length; i++){
            blackjackPlayers[i].resetHand();
            // Hit each player twice
            for(int j = 0; j < 2; j++){
                hitPlayer(blackjackPlayers[i]);
            }
        }

        if(!endgame){
            playing = true;
        }
        drawBoard();
    }

    private void drawBoard(){
        g.setFill(Color.DARKGREEN);
        g.fillRect(0,0, board.getWidth(), board.getHeight());
        g.setFill( Color.rgb(220,255,220) );
        g.setFont( Font.font(16) );

        for(int i = 0; i < blackjackPlayers.length; i++){
            BlackjackPlayer blackjackPlayer = blackjackPlayers[i];
            if(blackjackPlayer.playerIsDealer()){
                g.fillText(DEALER_LABEL, CARD_HORIZONTAL_BORDER, CARD_HORIZONTAL_BORDER);
            } else {
                g.fillText(
                        PLAYER_LABEL,
                        CARD_HORIZONTAL_BORDER,
                        CARD_HORIZONTAL_BORDER + MESSAGE_HEIGHT + (2 * CARD_VERTICAL_BORDER) + CARD_HEIGHT
                );
            }

            for(int j = 0; j < blackjackPlayer.getCardCount(); j++){
                int cardYPosition = 0;

                if(blackjackPlayer.playerIsDealer()){
                    cardYPosition = MESSAGE_HEIGHT;
                } else {
                    cardYPosition = (2 * CARD_VERTICAL_BORDER) + (2 * MESSAGE_HEIGHT) + CARD_HEIGHT;
                }

                if(j == 0 && blackjackPlayer.playerIsDealer() && !endgame){
                    drawCard(
                            null,
                            CARD_HORIZONTAL_BORDER,
                            cardYPosition
                    );
                } else {
                    drawCard(
                            blackjackPlayer.getCard(j),
                            CARD_HORIZONTAL_BORDER + j * CARD_DISPLAY_WIDTH,
                            cardYPosition
                    );
                }
            }
        }

        int playerScore = blackjackPlayers[1].getScore();
        String playerMessage = String.format(GAME_SCORE_LABEL, playerScore);
        if(!endgame){
            // The game is not yet over. Offer choice to hit or stay
            playerMessage += " " + GAME_CHOICE_LABEL;
        } else {
            playerMessage += " " + gameEndReason;
        }
        gameMessageLabel.setText(playerMessage);
    }

    // Most of this is from HighLowGUI.java
    private void drawCard(Card card, int x, int y){
        int cardRow, cardCol;
        if (card == null) {
            cardRow = 4;   // row and column of a face down card
            cardCol = 2;
        }
        else {
            cardRow = 3 - card.getSuit();
            cardCol = card.getValue() - 1;
        }
        double sx,sy;  // top left corner of source rect for card in cardImages
        sx = CARD_IMAGE_WIDTH * cardCol;
        sy = CARD_HEIGHT * cardRow;
        g.drawImage(
                cardImages,
                sx,
                sy,
                CARD_IMAGE_WIDTH,
                CARD_HEIGHT,
                x,
                y,
                CARD_IMAGE_WIDTH,
                CARD_HEIGHT
        );
    }

    private void hitButtonHandler(ActionEvent e){
        // Give the player a card
        hitPlayer(blackjackPlayers[1]);

        drawBoard();
    }

    private void hitPlayer(BlackjackPlayer player){
        player.takeCard(deck.dealCard());

        if(!player.playerIsDealer()){
            determineGameStatus();
        }
    }

    private void stayButtonHandler(ActionEvent e){
        endgame = true;
        determineGameStatus();
        finishGame();

        drawBoard();
    }

    private void newGameButtonHandler(ActionEvent e){
        startNewGame();
    }

    /**
     * Determine the player's game status. If it not yet Endgame, determine if the
     * player can continue. If Endgame has started, do the dealer's moves then determine
     * the player's status.
     */
    private void determineGameStatus(){
        /*
        If it is not yet endgame, only check the player's status
         */
        if(!endgame){
            if(blackjackPlayers[1].getCardCount() == 5 && blackjackPlayers[1].getScore() <= 21) {
                // This is the only time the player can immediately win
                blackjackPlayers[1].setWinner();
                gameEndReason = "You win with five cards totaling 21 or less!";
                finishGame();
            } else if(blackjackPlayers[1].getHasBusted()){
                gameEndReason = "You busted";
                blackjackPlayers[0].setWinner();
                finishGame();
            } else if (blackjackPlayers[1].getCardCount() == 5){
                finishGame();
            } else if (blackjackPlayers[1].getScore() == 21){
            /*
            Start the endgame process to confirm who wins
             */
                endgame = true;
                determineGameStatus();
            }
        } else {
            // Now that it's endgame, play the dealer and check status

            while(blackjackPlayers[0].getScore() <= 16){
                hitPlayer(blackjackPlayers[0]);
            }

            if(blackjackPlayers[0].getHasBusted()){
                // Player wins if the dealer busted
                blackjackPlayers[1].setWinner();
                gameEndReason = "Dealer busted. You win!";
            } else if(blackjackPlayers[0].getHasWon()){
                // Dealer wins if it gets blackjack, no matter what
                blackjackPlayers[0].setWinner();
                gameEndReason = "Dealer has 21. You lose.";
            } else {
                if(blackjackPlayers[0].getScore() >= blackjackPlayers[1].getScore()){
                    blackjackPlayers[0].setWinner();
                    gameEndReason = "Dealer has a higher or equal score. You lose";
                } else {
                    blackjackPlayers[1].setWinner();
                    gameEndReason = "Your score is higher. You win!";
                }
            }

            finishGame();
        }

    }

    /**
     * Finish up the game.
     */
    private void finishGame(){
        if(playing){
            hitMeButton.setDisable(true);
            stayMeButton.setDisable(true);

            endgame = true;
            playing = false;
            newGameMeButton.setDisable(false);
            betTextField.setDisable(false);

            blackjackPlayers[1].processWinnings();
            playerPotLabel.setText("You have $" + blackjackPlayers[1].getPlayerPot());
        }
    }
}
