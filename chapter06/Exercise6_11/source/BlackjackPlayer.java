public class BlackjackPlayer {
    private boolean isDealer = false;
    private BlackjackHand hand;
    private boolean hasBusted = false;
    private boolean hasWon = false;

    private int playerBet = 10;
    private int playerPot = 100;

    public boolean playerIsDealer(){
        return isDealer;
    }

    public BlackjackPlayer(){
        this(new BlackjackHand());
    }

    public BlackjackPlayer(BlackjackHand hand){
        this(hand, false);
    }

    // A player can only be made a dealer at instantiation
    public BlackjackPlayer(BlackjackHand hand, boolean isDealer){
        this.hand = hand;
        this.isDealer = isDealer;
    }

    public BlackjackPlayer(boolean isDealer){
        this(new BlackjackHand(), isDealer);
    }

    public BlackjackHand getHand(){
        return hand;
    }

    public int getCardCount(){
        return hand.getCardCount();
    }

    public Card getCard(int i){
        return hand.getCard(i);
    }

    public void resetHand(){
        hand = new BlackjackHand();
        hasWon = false;
        hasBusted = false;
    }

    public void takeCard(Card card){
        hand.addCard(card);
        if(getScore() > 21){
            hasBusted = true;
        } else if (getScore() == 21){
            hasWon = true;
        }
    }

    public void processWinnings(){
        if(hasWon){
            playerPot = Math.min(Integer.MAX_VALUE, playerPot + playerBet);
        } else {
            playerPot = Math.max(0, playerPot - playerBet);
        }
    }

    public int getScore(){
        return hand.getBlackjackValue();
    }

    public boolean getHasBusted() {
        return hasBusted;
    }

    public boolean getHasWon(){
        return hasWon;
    }

    public void setWinner() {
        hasWon = true;
    }

    public int getPlayerBet() {
        return playerBet;
    }

    public void setPlayerBet(int playerBet) {
        this.playerBet = playerBet;
    }

    public int getPlayerPot() {
        return playerPot;
    }
}
