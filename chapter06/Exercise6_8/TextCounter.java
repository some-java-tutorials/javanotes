import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class TextCounter extends Application {
    private TextArea textArea;
    private Label lineCountLabel;
    private Label wordCountLabel;
    private Label charCountLabel;

    private final String LC_HEADING = "Number of lines: ";
    private final String WC_HEADING = "Number of words: ";
    private final String CC_HEADING = "Number of chars: ";

    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage) {
        // Setup the TilePane
        FlowPane pane = new FlowPane(Orientation.VERTICAL);
        pane.setVgap(5);
        pane.setColumnHalignment(HPos.CENTER);

        pane.setId("mainWindow");

        // Setup TextArea
        textArea = new TextArea();

        // Create button
        Button button = new Button("Process the Text");
        button.setOnAction(this::processTextHandler);

        // Create Labels
        lineCountLabel = new Label(LC_HEADING);
        lineCountLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        wordCountLabel = new Label(WC_HEADING);
        wordCountLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        charCountLabel = new Label(CC_HEADING);
        charCountLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        // Add components to TilePane
        pane.getChildren().addAll(
                textArea,
                button,
                lineCountLabel,
                wordCountLabel,
                charCountLabel
        );

        // Setup scene
        Scene scene = new Scene(pane);
        scene.getStylesheets().add("css/style.css");
        stage.setScene(scene);
        stage.setTitle("Line/Word/Char Counter");

        stage.setResizable(false);

        textArea.requestFocus();

        stage.show();
    }

    private void processTextHandler(ActionEvent e){
        lineCountLabel.setText(LC_HEADING + getLineCount());
        charCountLabel.setText(CC_HEADING + getCharCount());
        wordCountLabel.setText(WC_HEADING + getWordCount());
    }

    private int getCharCount(){
        return textArea.getText().length();
    }

    private int getLineCount(){
        int lineCount = 1;
        String text = textArea.getText();
        for(int i = 0; i < text.length(); i++){
            if(text.charAt(i) == '\n'){
                lineCount++;
            }
        }

        return lineCount;
    }

    private int getWordCount(){
        int wordCount = 0;
        String text = textArea.getText();
        boolean wordStarted = false;
        for(int i = 0; i < text.length(); i++){
            if(!wordStarted && isWordChar(text.charAt(i))){
                wordStarted = true;
                wordCount++;
            } else if (!isWordChar(text.charAt(i))){
                wordStarted = false;
            }
        }

        return wordCount;
    }

    private boolean isWordChar(char c){
        return Character.isAlphabetic(c) || c == '\'' || c == '-';
    }
}
