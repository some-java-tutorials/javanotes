public class BlackjackPlayer {
    private boolean isDealer = false;
    private BlackjackHand hand;
    private boolean hasBusted = false;
    private boolean hasWon = false;

    public boolean playerIsDealer(){
        return isDealer;
    }

    public BlackjackPlayer(){
        this(new BlackjackHand());
    }

    public BlackjackPlayer(BlackjackHand hand){
        this(hand, false);
    }

    // A player can only be made a dealer at instantiation
    public BlackjackPlayer(BlackjackHand hand, boolean isDealer){
        this.hand = hand;
        this.isDealer = isDealer;
    }

    public BlackjackPlayer(boolean isDealer){
        this(new BlackjackHand(), isDealer);
    }

    public BlackjackHand getHand(){
        return hand;
    }

    public int getCardCount(){
        return hand.getCardCount();
    }

    public Card getCard(int i){
        return hand.getCard(i);
    }

    public void resetHand(){
        hand = new BlackjackHand();
        hasWon = false;
        hasBusted = false;
    }

    public void takeCard(Card card){
        hand.addCard(card);
        if(getScore() > 21){
            hasBusted = true;
        } else if (getScore() == 21){
            hasWon = true;
        }
    }

    public int getScore(){
        return hand.getBlackjackValue();
    }

    public boolean getGameStatus(){
        return hasWon && !hasBusted;
    }

    public boolean getHasBusted() {
        return hasBusted;
    }

    public boolean getHasWon(){
        return hasWon;
    }

    public boolean determineGameStatus(){
        if(hand.getBlackjackValue() == 21){
            hasWon = true;
            return hasWon;
        } else if(hand.getBlackjackValue() > 21){
            hasBusted = true;
        }

        // Player has not won, but they lost if they busted
        return false;
    }
}
