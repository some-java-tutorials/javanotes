import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;


public class RollDice extends Application {
    private PairOfDice pairOfDice;

    private boolean rolling = false;

    AnimationTimer timer;
    private static final int CANVAS_WIDTH = 100;
    private static final int CANVAS_HEIGHT = 100;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage){
        Canvas canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);

        BorderPane root = new BorderPane();

        Button rollButton = new Button("Roll");
        rollButton.setOnAction(this::rollButtonHandler);

        root.setCenter(canvas);
        root.setBottom(rollButton);
        BorderPane.setAlignment(rollButton, Pos.CENTER);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Roll the dice");
        stage.setResizable(false);

        GraphicsContext g = canvas.getGraphicsContext2D();

        // Draw dice when window first comes up
        // The PairOfDice are given the graphics context to use
        pairOfDice = new PairOfDice(g);
        pairOfDice.drawDice();

        stage.show();

        timer = new AnimationTimer() {
            long previousFrameTime;
            int rolls;
            public void handle( long time ) {
                if(rolling & time - previousFrameTime > 1e9/4){
                    if(rolls < 6){
                        pairOfDice.roll();
                        rolls += 1;
                    } else {
                        rolls = 0;
                        rolling = false;
                    }

                    previousFrameTime = time;
                }
            }
        };

        timer.start();
    }

    private void rollButtonHandler(ActionEvent e){
        if(!rolling){
            startRolling();
        }
    }

    private void startRolling(){
        rolling = true;
    }
}
