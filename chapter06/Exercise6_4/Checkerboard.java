import javafx.application.Application;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *  This file can be used to draw simple pictures.  In this case, it will draw a checkerboard.
 */
public class Checkerboard extends Application {
    private int sideLength;
    private GraphicsContext g;

    private final int BOARD_WIDTH = 400; // The width of the image.  You can modify this value!
    private final int BOARD_HEIGHT = 400; // The height of the image. You can modify this value!

    /**
     * Draws a picture.  The parameters width and height give the size 
     * of the drawing area, in pixels.  
     */
    public void drawPicture(int width, int height) {

        g.setFill(Color.WHITE);
        g.fillRect(0, 0, width, height); // First, fill the entire image with a background color.

        int rows = 8;
        int columns = 8;
        sideLength = width / rows;
        Color squareColor;
        
        for (int xIndex = 0; xIndex < rows; xIndex++) {    
            for (int yIndex = 0; yIndex < columns; yIndex++) {
                squareColor = Color.BLACK;
                if(
                    (yIndex % 2 == 0 && xIndex % 2 == 0) ||
                    (yIndex % 2 == 1 && xIndex % 2 == 1)
                    ){
                    squareColor = Color.RED;
                }
                g.setFill(squareColor);
                g.fillRect(xIndex * sideLength, yIndex * sideLength, sideLength, sideLength);
            }
        }

    } // end drawPicture()

    public void start(Stage stage) {
        Canvas canvas = new Canvas(BOARD_HEIGHT, BOARD_HEIGHT);
        // Set the class-wide graphics context
        g = canvas.getGraphicsContext2D();

        drawPicture(BOARD_WIDTH, BOARD_HEIGHT);

        // Setup event handlers
        canvas.setOnMouseClicked(this::mouseClicked);

        BorderPane root = new BorderPane(canvas);
        root.setStyle("-fx-border-width: 4px; -fx-border-color: #444");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Exercise 6.4"); // STRING APPEARS IN WINDOW TITLEBAR!
        stage.show();
        stage.setResizable(false);
    } 

    public static void main(String[] args) {
        launch();
    }

    private void mouseClicked(MouseEvent e){
        // Clear the Checkerboard
        drawPicture(BOARD_WIDTH, BOARD_HEIGHT);

        double x = e.getX();
        double y = e.getY();

        // Find the Checkerboard xIndex and yIndex from the mouse coordinates
        int xIndex = (int) x / sideLength;
        int yIndex = (int) y / sideLength;

        g.setStroke(Color.CYAN);
        g.setLineWidth(5);
        g.strokeRect(xIndex * sideLength, yIndex * sideLength, sideLength, sideLength);
    }

} // end Checkerboard
