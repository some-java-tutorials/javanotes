import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class SimpleCalc extends Application {
    private TextField mainInput;
    private Button enterButton;
    private Button clearButton;

    private StatCalc statCalc;
    private final String ENTRIES_HEADING = "Number of Entries:";
    private final String SUM_HEADING = "Sum:";
    private final String AVERAGE_HEADING = "Average:";
    private final String SD_HEADING = "Standard Deviation:";

    private String printLabel(String labelText){
        return String.format("%1$-20s", labelText);
    }

    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage){
        TilePane pane = new TilePane(Orientation.VERTICAL);
        pane.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
        pane.setPrefRows(6);
        pane.setVgap(1);

        Label headerLabel = new Label("Enter a number, click Enter:");
        headerLabel.setFont(new Font("Verdana", 18));
        headerLabel.setTextFill(Color.WHITE);
        headerLabel.setStyle("-fx-pref-width: 400px; -fx-alignment: center; -fx-padding: 5px 0px");

        // Setup form controls
        mainInput = new TextField();

        // Make StatCalc instance
        statCalc = new StatCalc();

        // Create buttons
        enterButton = new Button("Enter");
        clearButton = new Button("Clear");

        // Setup the display of components
        HBox formFields = new HBox(5, mainInput, enterButton, clearButton);
        HBox.setHgrow(enterButton, Priority.ALWAYS);
        HBox.setHgrow(clearButton, Priority.ALWAYS);
        enterButton.setMaxWidth(Double.POSITIVE_INFINITY);
        clearButton.setMaxWidth(Double.POSITIVE_INFINITY);

        Label entriesLabel = new Label(printLabel(ENTRIES_HEADING));
        entriesLabel.getStyleClass().add("displayLabel");
        entriesLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        Label sumLabel = new Label(printLabel(SUM_HEADING));
        sumLabel.getStyleClass().add("displayLabel");
        sumLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        Label averageLabel = new Label(printLabel(AVERAGE_HEADING));
        averageLabel.getStyleClass().add("displayLabel");
        averageLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        Label sdLabel = new Label(printLabel(SD_HEADING));
        sdLabel.getStyleClass().add("displayLabel");
        sdLabel.setMaxWidth(Double.POSITIVE_INFINITY);

        // Set button functions
        clearButton.setOnAction(e -> {
            mainInput.clear();
            mainInput.requestFocus();
        });

        enterButton.setOnAction(e -> {
            // Ensure numeric input
            try{
                statCalc.enter(Double.parseDouble(mainInput.getText()));

                entriesLabel.setText(ENTRIES_HEADING + " " + statCalc.getCount());
                sumLabel.setText(SUM_HEADING + " " + statCalc.getSum());
                averageLabel.setText(AVERAGE_HEADING + " " + statCalc.getMean());
                sdLabel.setText(SD_HEADING + " " + statCalc.getStandardDeviation());
            } catch (Exception exp) {
                mainInput.clear();
                mainInput.requestFocus();
            }
        });

        pane.getChildren().addAll(
                headerLabel,
                formFields,
                entriesLabel,
                sumLabel,
                averageLabel,
                sdLabel
        );

        // Create the scene
        Scene scene = new Scene(pane);
        scene.getStylesheets().add("css/style.css");
        stage.setScene(scene);
        stage.setTitle("Simple Calc GUI");

        stage.setResizable(false);

        stage.show();
    }
}