import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Die {
    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 6;
    private static final Color STROKE_COLOR = Color.BLACK;
    private static final Color FILL_COLOR = Color.WHITE;
    private static final Color DOT_COLOR = Color.BLACK;
    private static final int DICE_WIDTH = 30;
    private static final int DICE_HEIGHT = 30;
    private static final int DOT_WIDTH = 5;
    private static final int DOT_HEIGHT = 5;

    private GraphicsContext g;

    public Die(GraphicsContext g){
        this.g = g;
    }

    //private int value  = roll();
    private int value  = roll();

    private int x;
    private int y;

    public int getX(){
        return x;
    }
    public void setX(int val){
        x = val;
    }

    public int getY(){
        return y;
    }
    public void setY(int val){
        y = val;
    }

    public int getValue(){
        return value;
    }

    public int roll(){
        value = (int)(Math.random() * MAX_VALUE) + MIN_VALUE;
        return value;
    }

    public void draw(int x, int y){
        setX(x);
        setY(y);

        drawDiceBackground();
        drawDiceDots();
    }

    public void drawDiceBackground(){
        g.setFill(FILL_COLOR);
        g.fillRect(x, y, DICE_WIDTH, DICE_HEIGHT);

        g.setStroke(STROKE_COLOR);
        g.strokeRect(x, y, DICE_WIDTH, DICE_HEIGHT);
    }


    public void drawDiceDots(){
        switch(value){
            case 1:
                drawOne();
            break;
            case 2:
                drawTwo();
            break;
            case 3:
                drawThree();
            break;
            case 4:
                drawFour();
            break;
            case 5:
                drawFive();
            break;
            case 6:
                drawSix();
            break;
        }
    }

    /**
     * Return the coordinate that corresponds with placing the given coordinate
     * at the center of the dimension. This will determine exactly where the
     * object should be placed to be in the center of the given dimension.
     * 
     * @param coord
     * @param dimension
     * @return
     */
    private double centerOffset(double coord, double dimension){
        return (coord - (dimension / 2));
    }

    private void drawOne(){
        g.setFill(DOT_COLOR);
        g.fillOval(centerOffset(x , DOT_WIDTH) + ( (double)DICE_WIDTH / 2), centerOffset(y, DOT_HEIGHT) + ((double)DICE_HEIGHT / 2), DOT_WIDTH, DOT_HEIGHT);
    }

    private void drawTwo(){
        g.setFill(DOT_COLOR);
        g.fillOval(centerOffset(x , DOT_WIDTH) + 5, centerOffset(y, DOT_HEIGHT) + 5, DOT_WIDTH, DOT_HEIGHT);

        g.fillOval(centerOffset(x , DOT_WIDTH) + (DICE_WIDTH - 5), centerOffset(y, DOT_HEIGHT) + (DICE_HEIGHT - 5), DOT_WIDTH, DOT_HEIGHT);
    }

    private void drawThree(){
        drawOne();
        drawTwo();
    }

    private void drawFour(){
        drawTwo();
        drawOppositeTwo();
    }

    private void drawFive(){
        drawOne();
        drawFour();
    }

    private void drawSix(){
        drawFour();
        drawTwoBetweenFour();
    }

    private void drawOppositeTwo(){
        g.setFill(DOT_COLOR);
        g.fillOval(centerOffset(x , DOT_WIDTH) + 5, centerOffset(y, DOT_HEIGHT) + (DICE_HEIGHT - 5), DOT_WIDTH, DOT_HEIGHT);

        g.fillOval(centerOffset(x , DOT_WIDTH) + (DICE_WIDTH - 5), centerOffset(y, DOT_HEIGHT) + 5, DOT_WIDTH, DOT_HEIGHT);
    }

    private void drawTwoBetweenFour(){
        g.fillOval(centerOffset(x , DOT_WIDTH) + 5, centerOffset(y, DOT_HEIGHT) + ((double)DICE_HEIGHT / 2), DOT_WIDTH, DOT_HEIGHT);

        g.fillOval(centerOffset(x , DOT_WIDTH) + (DICE_WIDTH - 5), centerOffset(y, DOT_HEIGHT) + ((double)DICE_HEIGHT / 2), DOT_WIDTH, DOT_HEIGHT);
    }
}
