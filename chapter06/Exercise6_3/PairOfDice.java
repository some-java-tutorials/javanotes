import javafx.scene.canvas.GraphicsContext;

public class PairOfDice {

    private Die die1;
    private Die die2;

    private GraphicsContext g;

    public PairOfDice(GraphicsContext g){
        this.g = g;
        die1 = new Die(g);
        die2 = new Die(g);
    }

    public void roll() {
         die1.roll();
         die2.roll();

         drawDice();
    }

    public Die getDie1() {
        return die1;
    }

    public Die getDie2() {
        return die2;
    }

    public void drawDice(){
        die1.draw(10, 10);
        die2.draw(50, 60);
    }

    
}