import javafx.scene.canvas.Canvas;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;


public class RollDice extends Application {
    private Canvas canvas;
    private PairOfDice pairOfDice;
    private static final int CANVAS_WIDTH = 100;
    private static final int CANVAS_HEIGHT = 100;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage){
        canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);

        Pane root = new Pane(canvas);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Roll the dice");
        stage.setResizable(false);

        GraphicsContext g = canvas.getGraphicsContext2D();

        pairOfDice = new PairOfDice(g);

        pairOfDice.drawDice();

        canvas.setOnMouseClicked(this::clickHandler);

        stage.show();
    }

    private void clickHandler(MouseEvent e){
        if(e.getButton() == MouseButton.PRIMARY){
            pairOfDice.roll();
        }

    }
}
