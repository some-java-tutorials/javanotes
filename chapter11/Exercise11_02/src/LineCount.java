import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LineCount {
    public static void main(String[] args) {
        for(String filePath : args){
            try{
                CountLines(new File(filePath));
            } catch (FileNotFoundException e){
                System.out.println("Could not find file " + filePath);
            }
        }
    }

    private static void CountLines(File file) throws FileNotFoundException {
        Scanner in;
        if(!file.isFile()){
            System.out.println(file.getAbsolutePath() + " is not a file");
            return;
        }

        in = new Scanner(file);
        int lineCount = 0;

        while(in.hasNextLine()){
            in.nextLine();
            lineCount++;
        }

        System.out.println(file.getAbsolutePath() + ": " + lineCount);
    }
}
