import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

public class MiniWebServerClient extends Application {

    /**
     * Port number on server, if none is specified on the command line.
     */
    static final int DEFAULT_PORT = 1728;
    static final String DEFAULT_HOST = "localhost";
    /**
     * Handshake string. Each end of the connection sends this  string to the
     * other just after the connection is opened.  This is done to confirm that
     * the program on the other side of the connection is expecting to connect
     * to this web server.
     */
    static final String HANDSHAKE = "MiniWebServer";
    static final String INDEX_COMMAND = "INDEX";
    static final String GET_COMMAND = "GET";

    private Socket connection;      // For communication with the server.
    private BufferedReader incoming;  // Stream for receiving data from server.
    private String messageIn;         // A message received from the server.
    private PrintWriter outgoing;     // Stream for sending data to server.

    private String selectedFile = "";
    private ObservableList<String> fileListData = FXCollections.observableArrayList();
    private ListView<String> fileListView = new ListView<>();
    private IndicatorLabel connectionIndicatorLabel = new IndicatorLabel();
    private Button indexButton = new Button("INDEX");
    private Button getButton = new Button("GET");

    private BooleanProperty isConnected = new SimpleBooleanProperty(false);

    private Stage mainStage;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        mainStage = stage;
        FlowPane pane = new FlowPane(Orientation.VERTICAL);
        pane.setVgap(5);
        pane.setColumnHalignment(HPos.CENTER);

        pane.setId("mainPane");
        fileListView.setId("fileList");

        connectionIndicatorLabel.setId("connectionIndicatorLabel");

        indexButton.setOnAction(this::indexButtonHandler);
        getButton.setOnAction(this::getButtonHandler);

        HBox buttonBox = new HBox(5, indexButton, getButton);
        HBox.setHgrow(indexButton, Priority.ALWAYS);
        HBox.setHgrow(getButton, Priority.ALWAYS);
        indexButton.setMaxWidth((Double.POSITIVE_INFINITY));
        getButton.setMaxWidth((Double.POSITIVE_INFINITY));
        getButton.setDisable(true);

        isConnected.addListener((observableValue, oldVal, newVal) -> {
            if(newVal){
                connectionIndicatorLabel.turnOn();
            } else {
                connectionIndicatorLabel.turnOff();
            }
        });

        fileListView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldVal, newVal) -> {
            selectedFile = newVal;
        });

        pane.getChildren().addAll(
                connectionIndicatorLabel,
                fileListView,
                buttonBox
        );

        Scene scene = new Scene(pane);
        scene.getStylesheets().add("css/style.css");
        stage.setScene(scene);
        stage.setTitle("MiniWebServer Client");

        stage.setResizable(false);
        stage.show();
    }

    private void indexButtonHandler(ActionEvent actionEvent) {
        sendIndexCommand();
    }

    private void getButtonHandler(ActionEvent actionEvent){
        sendGetCommand();
    }

    private void sendIndexCommand(){
        connect();
        outgoing.println(INDEX_COMMAND);
        outgoing.flush();

        fileListData.clear();

        try {
            messageIn = incoming.readLine();
            while(messageIn != null){
                fileListData.add(messageIn);
                messageIn = incoming.readLine();
            }

            fileListView.setItems(fileListData);
            getButton.setDisable(false);
        } catch (IOException e) {
            System.out.println("Error sending " + INDEX_COMMAND + " command: " + e.getMessage());
        }
    }

    private void sendGetCommand(){
        connect();

        if(!selectedFile.isEmpty()){
            selectedFile = selectedFile.trim();
            outgoing.println(GET_COMMAND + " " + selectedFile);
            outgoing.flush();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save file...");
            fileChooser.setInitialDirectory(new File("download"));
            File selectedFile = fileChooser.showSaveDialog(mainStage);

            if(selectedFile != null){
                PrintWriter out;
                try {
                    FileWriter stream = new FileWriter(selectedFile);
                    out = new PrintWriter( stream );
                }
                catch (Exception e) {
                    // Most likely, user doesn't have permission to write the file.
                    Alert errorAlert = new Alert(Alert.AlertType.ERROR,
                            "Sorry, but an error occurred while trying to open the file for output.");
                    errorAlert.showAndWait();
                    return;
                }

                try {
                    messageIn = incoming.readLine();
                    while(messageIn != null){
                        if (Objects.equals(messageIn, "OK")) {
                            messageIn = incoming.readLine();
                            continue;
                        }
                        out.println(messageIn);
                        messageIn = incoming.readLine();
                    }
                    out.flush();
                    out.close();
                    if (out.checkError())   // (need to check for errors in PrintWriter)
                        throw new IOException("Error check failed.");
                }
                catch (Exception e) {
                    Alert errorAlert = new Alert(Alert.AlertType.ERROR,
                            e.getMessage());
                    errorAlert.showAndWait();
                }
            }
        }
    }

    private void connect(){
        /* Open a connection to the server.  Create streams for
         communication and exchange the handshake. */

        try {
            connectionIndicatorLabel.turnOff();
            System.out.println("Connecting to " + DEFAULT_HOST + " on port " + DEFAULT_PORT);
            connection = new Socket(DEFAULT_HOST, DEFAULT_PORT);
            incoming = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()) );
            outgoing = new PrintWriter(connection.getOutputStream());
            outgoing.println(HANDSHAKE);  // Send handshake to server.
            outgoing.flush();
            messageIn = incoming.readLine();  // Receive handshake from server.
            if (! messageIn.equals(HANDSHAKE) ) {
                connectionIndicatorLabel.setError();
                throw new IOException("Connected program is not MiniWebServer!");
            }
            connectionIndicatorLabel.turnOn();

        }
        catch (Exception e) {
            System.out.println("An error occurred while opening connection.");
            System.out.println(e);
            connectionIndicatorLabel.setError();
        }
    }
}
