import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

public class IndicatorLabel extends Label {
    static final Color ON_COLOR = Color.GREEN;
    static final Color OFF_COLOR = Color.GRAY;

    static final Color ERROR_COLOR = Color.RED;

    public void turnOn(){
        setBackground(new Background(new BackgroundFill(ON_COLOR, null, null)));
    }
    public void turnOff(){
        setBackground(new Background(new BackgroundFill(OFF_COLOR, null, null)));
    }
    public void setError(){
        setBackground(new Background(new BackgroundFill(ERROR_COLOR, null, null)));
    }
}
