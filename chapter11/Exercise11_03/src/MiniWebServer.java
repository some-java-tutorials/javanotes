import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class MiniWebServer {
    /**
     * Port to listen on, if none is specified on the command line.
     */
    static final int DEFAULT_PORT = 1728;

    /**
     * Handshake string. Each end of the connection sends this  string to the
     * other just after the connection is opened.  This is done to confirm that
     * the program on the other side of the connection is expecting to connect
     * to this web server.
     */
    static final String HANDSHAKE = "MiniWebServer";

    static final String INDEX_COMMAND = "INDEX";
    static final String GET_COMMAND = "GET";

    private ServerSocket listener;  // Listens for a connection request.
    private Socket connection;      // For communication with the client.
    private BufferedReader incomingCommand;  // Stream for receiving data from client.
    private PrintWriter outgoing;     // Stream for sending data to client.
    private String messageIn;         // A message received from the client.

    private Scanner in;
    private ArrayList<String> command;
    private String queriedFile;


    public static void main(String[] args) {
        MiniWebServer server = new MiniWebServer();
        server.init();
    }

    public void init(){
        setupConnection();
        while(true){
            listen();
        }
    }

    private void setupConnection(){
        try {
            listener = new ServerSocket(DEFAULT_PORT);
            System.out.println("Listening on port " + listener.getLocalPort());
        } catch (IOException e) {
            System.out.println("Could not setup connection");
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private void listen(){
        try {
            connection = listener.accept();
            incomingCommand = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()) );

            outgoing = new PrintWriter(connection.getOutputStream());
            outgoing.println(HANDSHAKE);  // Send handshake to client.
            outgoing.flush();
            messageIn = incomingCommand.readLine();  // Receive handshake from client.
            if (! HANDSHAKE.equals(messageIn) ) {
                throw new Exception("Connected program is not a MiniWebServer client!");
            }

        } catch(IOException e){
            System.out.println("Could not start server");
            System.out.println(e.getMessage());
        } catch(Exception e){
            System.out.println("An error occurred");
            System.out.println(e.getMessage());
        }

        try {
            while (true) {
                System.out.println("WAITING...");
                messageIn = incomingCommand.readLine();
                if(messageIn.isEmpty()){
                    throw new Exception("Command message is empty");
                }

                command = new ArrayList<>(List.of(messageIn.trim().split("\\s")));

                if(command.isEmpty()){
                    throw new Exception("Command list is empty");
                }

                if(command.size() == 1 && Objects.equals(command.get(0), INDEX_COMMAND)){
                    outputIndexCommand();
                }

                if(command.size() == 2 && Objects.equals(command.get(0), GET_COMMAND)){
                    outputGetCommand();
                }

                outgoing.flush(); // Make sure the data is sent!
                connection.close();
                if (outgoing.checkError()) {
                    throw new IOException("Error occurred while transmitting message.");
                }
            }
        }
        catch (Exception e) {
            System.out.println("Connection ended.");
            System.out.println("Error:  " + e);
        }
    }

    private void outputIndexCommand(){
        DirectoryList.ListFiles(outgoing);
    }

    private void outputGetCommand(){
        queriedFile = command.get(1);
        File file = new File(DirectoryList.DIRECTORY, queriedFile);
        if(file.isFile()){
            try {
                in = new Scanner(file);
                outgoing.println("OK");
                while(in.hasNextLine()){
                    outgoing.println(in.nextLine());
                }
            } catch (FileNotFoundException e) {
                outgoing.println("ERROR");
                outgoing.println("Could not find file " + file.getPath());
            }
        } else {
            outgoing.println("ERROR");
            outgoing.println("Is not a file: " + file.getPath());
        }
    }
}
