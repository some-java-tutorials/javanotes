import java.io.File;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/**
 * This program lists the files in a directory specified by
 * the user.  The user is asked to type in a directory name.
 * If the name entered by the user is not a directory, a
 * message is printed and the program ends.
 */
public class DirectoryList {
    static final String DIRECTORY = "zom_files";

    static void ListFiles(PrintWriter out){
        File directory;        // File object referring to the directory.
        String[] files;        // Array of file names in the directory.
        directory = new File(DIRECTORY);

        if (!directory.isDirectory()) {
            if (!directory.exists())
                out.println("There is no such directory: " + directory.getPath());
            else
                out.println("That file is not a directory: " + directory.getPath());
        }
        else {
            files = directory.list();
            assert files != null;
            for (String file : files) out.println("   " + file);
        }
    }

} // end class DirectoryList
