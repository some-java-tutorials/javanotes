import java.io.File;
import java.util.Scanner;

/**
 * This program lists the files in a directory specified by
 * the user.  The user is asked to type in a directory name.
 * If the name entered by the user is not a directory, a
 * message is printed and the program ends.
 */
public class DirectoryList {
    public static void main(String[] args) {
        String directoryName;  // Directory name entered by the user.
        File directory;        // File object referring to the directory.
        Scanner scanner;       // For reading a line of input from the user.

        scanner = new Scanner(System.in);  // scanner reads from standard input.

        System.out.print("Enter a directory name: ");
        directoryName = scanner.nextLine().trim();

        System.out.println("Files in directory \"" + directoryName + "\":");
        directory = new File(directoryName);
        ListFiles(directory, 0);

    } // end main()

    public static void ListFiles(File path, int pathLevels){
        String[] files;        // Array of file names in the directory.

        if (!path.isDirectory()) {
            if (!path.exists())
                System.out.println("There is no such directory!");
            else
                System.out.println("That file is not a directory.");
        }
        else {
            files = path.list();
            assert files != null;
            for (String file : files){
                for(int i = 0; i < pathLevels; i++){
                    System.out.print('\t');
                }
                System.out.println("   " + file);
                File childPath = new File(path, file);
                if(childPath.isDirectory()){
                    ListFiles(childPath, pathLevels + 1);
                }
            }
        }
    }

} // end class DirectoryList
