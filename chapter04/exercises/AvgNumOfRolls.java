package exercises;

public class AvgNumOfRolls {
    public static void main(String[] args) {
        int TOTAL_START = 2;
        int TOTAL_END = 12;
        int NUM_OF_TRIALS = 10000;

        System.out.println("Total On Dice     Average Number of Rolls");
        System.out.println("-------------     -----------------------");
        for(int i = TOTAL_START; i <= TOTAL_END; i++){
            int totalSum = 0;
            double avg = 0;
            for(int j = 1; j <= NUM_OF_TRIALS; j++){
                totalSum += RollDice.guessRoll(i);
            }
            avg = totalSum / ((double) NUM_OF_TRIALS);
            System.out.printf("%13d%13.4f%n", i, avg);
        }
    }
}
