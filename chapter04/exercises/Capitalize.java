package exercises;

import textio.TextIO;

public class Capitalize {
    public static void main(String[] args) {
        System.out.print("Enter the sentence to capitalize: ");

        print(TextIO.getln());
    }
    public static void print(String phrase){
        for(int i = 0; i < phrase.length(); i++){
            int nextCharIndex = i + 1;
            int prevCharIndex = i - 1;
            boolean firstLetterInWord = false;
            Character currentChar = phrase.charAt(i);

            if(prevCharIndex < 0 ||
                !Character.isLetter(phrase.charAt(prevCharIndex))
                && Character.isLetter(currentChar)
                ){
                firstLetterInWord = true;
            }

            
            if(firstLetterInWord &&
                nextCharIndex < phrase.length() &&                 
                Character.isLetter(phrase.charAt(nextCharIndex))
                ){
                currentChar = Character.toUpperCase(currentChar);
            }
            System.out.print(currentChar);
        }
    } 
}
