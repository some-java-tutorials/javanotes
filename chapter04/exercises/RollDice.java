package exercises; 

public class RollDice{
    public static int[] dice = new int[2];
    public static void main(String[] args) {
        int numOfRoles;
        System.out.println("Rolling for snake eyes...");
        numOfRoles = guessRoll(2);
        System.out.printf("You got snake eyes after %d rolls!%n", numOfRoles);
    }
    /**
     * Roll the dice
     * @return the total after rolling dice
     */
    public static int roll(){
        int total = 0;
        for(int i = 0; i < dice.length; i++){
            dice[i] = ((int) (6 * Math.random())) + 1;
            total += dice[i];
        }
        return total;
    }
    /**
     * 
     * @param guess The amount the user is rolling for
     * @return The number of times the dice was rolled before the guess was correct
     */
    public static int guessRoll(int guess){
        int numOfRoles = 0;
        while(roll() != guess){
            numOfRoles++;
        }
        return numOfRoles;
    }
}