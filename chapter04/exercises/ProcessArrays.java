package exercises;

import java.util.Arrays;

public class ProcessArrays{
    public static final ArrayProcessor max = (a) -> {
        Arrays.sort(a);
        return a[a.length - 1];
    };
    public static final ArrayProcessor min = (a) -> {
        Arrays.sort(a);
        return a[0];
    };
    public static final ArrayProcessor sum = (a) -> {
        int localSum = 0;
        for(int i = 0; i < a.length; i++){
            localSum += a[i];
        }
        return localSum;
    };

    public static final ArrayProcessor avg = (a) -> {
        int localSum = 0;
        for(int i = 0; i < a.length; i++){
            localSum += a[i];
        }
        return localSum / (double) a.length;
    };

    public static ArrayProcessor counter(double value){        
        return (a) -> {
            int count = 0;
            for(int i = 0; i < a.length; i++){
                if(value == a[i]){
                    count++;
                }
            }
            return count;
        };
    }
    public static void main(String[] args) {
        double[] testArray = { 70, -23, 5, 25, 11, 2, 25, 80, 25, -23 };
        System.out.printf("Max: %1.1f%n", max.apply(testArray));
        System.out.printf("Min: %1.1f%n", min.apply(testArray));
        System.out.printf("Sum: %1.1f%n", sum.apply(testArray));
        System.out.printf("Avg: %1.1f%n", avg.apply(testArray));

        System.out.printf("Num of times 25 appears: %1.1f", counter(25).apply(testArray));
    }
}