package exercises;

import textio.TextIO;

public class AdditionQuiz {
    private static final int QUIZ_LENGTH = 10;
    private static final int OPERAND_MAX = 50;
    static int[] firstOperands = new int[QUIZ_LENGTH];
    static int[] secondOperands = new int[QUIZ_LENGTH];
    static int[] answers = new int[QUIZ_LENGTH];

    static void createQuiz(){
        for(int i = 0; i < QUIZ_LENGTH; i++){
            firstOperands[i] = (int) (OPERAND_MAX * Math.random()) + 1;
            secondOperands[i] = (int) (OPERAND_MAX * Math.random()) + 1;
        }
    }

    static void doQuiz(){
        boolean continueQuiz = false;
        int score = 0;
        int correctAnswer = 0;

        createQuiz();

        do{
            for(int i = 0; i < answers.length; i++){
                System.out.printf("%d + %d = ", firstOperands[i], secondOperands[i]);
                answers[i] = TextIO.getlnInt();

                correctAnswer = firstOperands[i] + secondOperands[i];
                if(answers[i] == correctAnswer){
                    System.out.println("Correct!");
                    score++;
                } else {
                    System.out.printf("The answer is: %d%n", correctAnswer);
                }
            }
            System.out.printf("Score: %d%n", score);
            System.out.println("Do quiz again?");
            continueQuiz = TextIO.getlnBoolean();
        }while(continueQuiz);   
    }
    public static void main(String[] args) {
        doQuiz();
    }
}
