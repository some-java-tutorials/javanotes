package exercises;

import javafx.scene.paint.Color;

/**
 * This program opens a window full of randomly colored squares.  A "disturbance"
 * moves randomly around in the window, randomly changing the color of each
 * square that it visits.  The program runs until the user closes the window.
 */
public class MosaicOfLife {

    final static int ROWS = 20;        // Number of rows in mosaic.
    final static int COLUMNS = 30;     // Number of columns in mosaic.
    final static int SQUARE_SIZE = 20; // Size of each square in mosaic.

    static int currentRow;    // Row currently containing the disturbance.
    static int currentColumn; // Column currently containing disturbance.

    /**
     * The main program creates the window, fills it with random colors,
     * and then moves the disturbances in a random walk around the window
     * as long as the window is open.
     */
    public static void main(String[] args) {
        Mosaic.open( ROWS, COLUMNS, SQUARE_SIZE, SQUARE_SIZE );
        fillWithRandomColors();
        currentRow = ROWS / 2;   // start at center of window
        currentColumn = COLUMNS / 2;
        while (true) {
            selectRandomSquare();
            assimilateNeighbor();
            Mosaic.delay(5);  // Remove this line to speed things up!
        }
    }  // end main

    /**
     * Fills the window with randomly colored squares.
     * Precondition:   The mosaic window is open.
     * Postcondition:  Each square has been set to a random color. 
     */
    static void fillWithRandomColors() {
        for (int row=0; row < ROWS; row++) {
            for (int column=0; column < COLUMNS; column++) {
                changeToRandomColor(row, column);  
            }
        }
    }  // end fillWithRandomColors

    /**
     * Changes one square to a new randomly selected color.
     * Precondition:   The specified rowNum and colNum are in the valid range
     *                 of row and column numbers.
     * Postcondition:  The square in the specified row and column has
     *                 been set to a random color.
     * @param rowNum the row number of the square, counting rows down
     *      from 0 at the top
     * @param colNum the column number of the square, counting columns over
     *      from 0 at the left
     */
    static void changeToRandomColor(int rowNum, int colNum) {
        int red = (int)(256*Math.random());    // Choose random levels in range
        int green = (int)(256*Math.random());  //     0 to 255 for red, green, 
        int blue = (int)(256*Math.random());   //     and blue color components.
        Mosaic.setColor(rowNum,colNum,red,green,blue);  
    }  // end changeToRandomColor

    static void selectRandomSquare(){
        currentRow = (int) (ROWS * Math.random());
        currentColumn = (int) (COLUMNS * Math.random());
    }

    static void assimilateNeighbor(){
        int directionNum; // Randomly set to 0, 1, 2, or 3 to choose direction.
        directionNum = (int)(4*Math.random());

        int neighborRow = currentRow;
        int neighborColumn = currentColumn;

        Color currentColor = Mosaic.getColor(currentRow, currentColumn);
        
        switch (directionNum) {
        case 0:  // move up 
            neighborRow--;
            if (neighborRow < 0)
                neighborRow = ROWS - 1;
            break;
        case 1:  // move right
            neighborColumn++;
            if (neighborColumn >= COLUMNS)
                neighborColumn = 0;
            break; 
        case 2:  // move down
            neighborRow++;
            if (neighborRow >= ROWS)
                neighborRow = 0;
            break;
        case 3:  // move left  
            neighborColumn--;
            if (neighborColumn < 0)
                neighborColumn = COLUMNS - 1;
            break; 
        }

        Mosaic.setColor(neighborRow, neighborColumn, currentColor);
    }

} // end class MosaicOfLife
