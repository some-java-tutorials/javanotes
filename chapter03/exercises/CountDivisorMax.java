public class CountDivisorMax{
    public static void main(String[] args) {
        final int START = 1;
        final int END = 10000;
        int currentDivisorCount = 0;
        int lastMaxDivisorCount = 0;
        int maxNumber = START;

        for(int i = START; i < END; i++){
            for(int j = i; j >= START; j--){
                if(i % j == 0){
                    if(i == 9240){
                        System.out.printf("%d divides by %d evenly", i, j);
                        System.out.println();
                    }
                    
                    currentDivisorCount++;
                    if(currentDivisorCount >= lastMaxDivisorCount){
                        lastMaxDivisorCount = currentDivisorCount;                        
                        maxNumber = i;
                    }
                }                
            }
            currentDivisorCount = 0;
        }
        System.out.printf("Last maximum number of divisors found: %d", lastMaxDivisorCount);
        System.out.println();
        System.out.printf("Number it was found for: %d", maxNumber);
    }
}