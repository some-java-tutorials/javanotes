package exercises;

import textio.TextIO;

public class CitySales {
    public static void main(String[] args) {
        TextIO.readFile("exercises/sales.dat");
        double salesTotal = 0;
        int numOfUnavailable = 0;
        int lineNumber = 0;

        do{
            lineNumber++;
            String line = TextIO.getln();
            String[] lineData = line.split(":");
            String label = "";
            String info = "";

            try{
                label = lineData[0].trim().toLowerCase();
                info = lineData[1].trim().toLowerCase();
                if(info.equals("no report received")){
                    numOfUnavailable++;
                    continue;
                }
                salesTotal += Double.parseDouble(info);
            } catch (ArrayIndexOutOfBoundsException e){
                System.out.printf("Could not read data: %s on line %d", line, lineNumber);
                System.out.println();
            } catch (NumberFormatException e){
                System.out.printf("Could not %s as a number from: %s on line %d", info, label, lineNumber);
                System.out.println();
            }
        }while(!TextIO.eof());

        System.out.printf("The sales total is %1.2f", salesTotal);
        System.out.println();
        System.out.printf("Number of cities without reports: %d", numOfUnavailable);
        System.out.println();
    }
}
