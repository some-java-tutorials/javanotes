package exercises;

import textio.TextIO;

public class Calculator {
    public static void main(String[] args) {
        double firstNumber, secondNumber, answer;
        char operator;

        System.out.println("Enter a simple arithmetic expression to evaluate, then press Enter");
        System.out.println("E.g. 17 + 3 and 3.14159 * 4.7");
        System.out.println("The general format is [number] [operator] [number]");
        System.out.println("Whitespace will be disregarded");
        System.out.println("If 0 is entered as the first number, the program will end");
        while(true){           
            firstNumber = TextIO.getDouble();

            if(firstNumber == 0){
                break;
            }
            operator = TextIO.getChar();
            secondNumber = TextIO.getDouble();

            switch(operator){
                case '+':
                    answer = firstNumber + secondNumber;
                break;
                case '-':
                    answer = firstNumber - secondNumber;
                break;
                case '*':
                    answer = firstNumber * secondNumber;
                break;
                case '/':
                    answer = firstNumber / secondNumber;
                break;
                default:
                    System.out.printf("Unknown operator: %s", operator);
                    System.out.println();
                    continue;
            }
            System.out.printf("The answer is %1f", answer);
            System.out.println();
        }        
    }
}
