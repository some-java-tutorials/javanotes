import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *  This file can be used to draw simple pictures.  In this case, it will draw a checkerboard.
 */
public class Checkerboard extends Application {

    /**
     * Draws a picture.  The parameters width and height give the size 
     * of the drawing area, in pixels.  
     */
    public void drawPicture(GraphicsContext g, int width, int height) {

        g.setFill(Color.WHITE);
        g.fillRect(0, 0, width, height); // First, fill the entire image with a background color.

        int rows = 8;
        int columns = 8;
        int sideLength = width / rows;
        Color squareColor;
        
        for (int xIndex = 0; xIndex < rows; xIndex++) {    
            for (int yIndex = 0; yIndex < columns; yIndex++) {
                squareColor = Color.BLACK;
                if(
                    (yIndex % 2 == 0 && xIndex % 2 == 0) ||
                    (yIndex % 2 == 1 && xIndex % 2 == 1)
                    ){
                    squareColor = Color.RED;
                }
                g.setFill(squareColor);
                g.fillRect(xIndex * sideLength, yIndex * sideLength, sideLength, sideLength);
            }
        }

    } // end drawPicture()

    public void start(Stage stage) {
        int width = 400;   // The width of the image.  You can modify this value!
        int height = 400;  // The height of the image. You can modify this value!
        Canvas canvas = new Canvas(width,height);
        drawPicture(canvas.getGraphicsContext2D(), width, height);
        BorderPane root = new BorderPane(canvas);
        root.setStyle("-fx-border-width: 4px; -fx-border-color: #444");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Exercise 3.8"); // STRING APPEARS IN WINDOW TITLEBAR!
        stage.show();
        stage.setResizable(false);
    } 

    public static void main(String[] args) {
        launch();
    }

} // end Checkerboard
