package exercises;
import textio.TextIO;

public class WordLines {
    public static void main(String[] args) {
        System.out.println("Please enter the input text");
        String word;

        do {
            word = TextIO.getWord();
            StringBuilder newWord = new StringBuilder();
            // Remove any non-apostrophe
            for(int i = 0; i < word.length(); i++){
                char c = word.charAt(i);
                if(Character.isLetter(word.charAt(i)) || word.charAt(i) == '\''){
                    newWord.append(c);
                }
            }
            System.out.println(newWord);
        } while(!TextIO.eoln());
    }
}
