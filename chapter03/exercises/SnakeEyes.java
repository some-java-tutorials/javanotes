package exercises;

public class SnakeEyes {
    public static void main(String[] args) {
        SnakeEyes.roll();
    }
    public static void roll(){
        boolean noSnakeEyes = true;
        int numOfRolls = 0;
        while(noSnakeEyes){
            int[] die = new int[2];
            for(int i = 0; i < die.length; i++){
                die[i] = (int)(Math.random() * 6) + 1;
            }
            numOfRolls++;
            if(die[0] == 1 && die[1] == 1){
                noSnakeEyes = false;
            }
        }
        System.out.printf("Number of rolls until Snake Eyes: %s", numOfRolls);
    }
}
