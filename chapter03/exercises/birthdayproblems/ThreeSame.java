public class ThreeSame {
    public static void main(String[] args) {
        int[] used; 

        int count;       // The number of people who have been checked.
    
        used = new int[365];
    
        count = 0;
    
        while (true) {
                // Select a birthday at random, from 0 to 364.    
            int birthday;  // The selected birthday.
            birthday = (int)(Math.random()*365);
            count++;
    
            System.out.printf("Person %d has birthday number %d%n", count, birthday);
    
            used[birthday]++;
            if(used[birthday] == 3){
                System.out.printf("Person %d shares a birthday with two others", count);
                break;
            }
        } // end while            
    }
}
