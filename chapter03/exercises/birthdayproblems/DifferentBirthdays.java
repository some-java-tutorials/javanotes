public class DifferentBirthdays {
    public static void main(String[] args) {
        int[] used; 

        int count;       // The number of people who have been checked.
    
        used = new int[365];
    
        count = 0;
        int numOfIdenticalBirthdays = 0;
    
        while (true) {
            if(count >= used.length){
                break;
            }
                // Select a birthday at random, from 0 to 364.    
            int birthday;  // The selected birthday.
            birthday = (int)(Math.random()*365);
            count++;
    
            System.out.printf("Person %d has birthday number %d%n", count, birthday);
    
            if(used[birthday] >= 1){
                numOfIdenticalBirthdays++;
            }
            used[birthday]++;
        } // end while 
        System.out.printf("Unique birthdays: %d%n", used.length - numOfIdenticalBirthdays);
    }    
}
