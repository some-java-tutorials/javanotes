public class AtLeastOne {
    public static void main(String[] args) {
        int[] used; 

        int count;       // The number of people who have been checked.
    
        used = new int[365];
    
        count = 0;
        int numOfIdenticalBirthdays = 0;
        boolean everyPerson = false;
    
        while (true) {
                // Select a birthday at random, from 0 to 364.    
            int birthday;  // The selected birthday.
            birthday = (int)(Math.random()*365);
            count++;
    
            System.out.printf("Person %d has birthday number %d%n", count, birthday);
    
            used[birthday]++;

            everyPerson = true;
            for(int i = 0; i < used.length; i++){                
                if(used[i] == 0){
                    everyPerson = false;
                }
            }
            if(everyPerson){
                break;
            }
        } // end while 
        System.out.printf("After Person %d, all %d days have at least one birthday", count, used.length);
    }
}
