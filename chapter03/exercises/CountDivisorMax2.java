package exercises;

public class CountDivisorMax2{
    public static void main(String[] args) {
        final int START = 1;
        final int END = 10000;
        int currentDivisorCount = 0;
        int lastMaxDivisorCount = 0;
        int maxNumber = START;
        int[] maxNumbers = new int[END];

        for(int i = START; i < END; i++){
            for(int j = i; j >= START; j--){
                if(i % j == 0){
                    currentDivisorCount++;
                    if(currentDivisorCount >= lastMaxDivisorCount){
                        lastMaxDivisorCount = currentDivisorCount;                        
                        maxNumber = i;
                        maxNumbers[i - 1] = currentDivisorCount;
                    }
                }                
            }
            currentDivisorCount = 0;
        }       

        System.out.println("Among integers between 1 and 10000,");
        System.out.printf("The maximum number of divisors was: %d%n", lastMaxDivisorCount);
        System.out.println("Numbers with that many divisors include:");
        for(int i = 0; i < maxNumbers.length; i++){
            if(maxNumbers[i] == lastMaxDivisorCount){
                System.out.printf("%7d%n", i + 1);
            }
        }
    }
}