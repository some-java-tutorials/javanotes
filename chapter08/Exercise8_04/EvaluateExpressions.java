import textio.TextIO;

public class EvaluateExpressions {
    private static Expr expr;
    private static String userInput = "";

    public static void main(String[] args) {

        boolean validExpression = false;

        while(userWantsToContinue()){
            while(!validExpression){
                TextIO.putln("Enter an expression (e.g) x^2 or sin(x)+3*x, Enter 'q' to quit: ");
                String expression = TextIO.getln();

                if(expression.equals("q")){
                    return;
                }
                try{
                    expr = new Expr(expression);
                    validExpression = true;
                } catch (IllegalArgumentException e){
                    TextIO.putln(e.getMessage());
                }
            }

            double userNumber = 0;

            while(true){
                TextIO.putln("Enter a value for x, or 'q' to quit, or 's' to skip");

                userInput = TextIO.getln();
                try{
                    userNumber = Double.parseDouble(userInput);
                } catch (NumberFormatException e){
                    if(userInput.equalsIgnoreCase("q")){
                        // User wants to quit
                        return;
                    }

                    if(userInput.equalsIgnoreCase("s")){
                        // User wants to skip to entering an equation

                        // Reset the expression check
                        validExpression = false;
                        break;
                    }
                }

                double value = expr.value(userNumber);

                if(Double.isNaN(value)){
                    TextIO.putln("No real number solution for x = " + userInput);
                } else {
                    TextIO.putln(value);
                }
            }
        }
    }

    private static boolean userWantsToContinue(){
        return !userInput.equals("q");
    }
}
