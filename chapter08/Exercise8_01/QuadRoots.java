import textio.TextIO;

public class QuadRoots {
    public static void main(String[] args) {
        double A, B, C, largestRoot;
        TextIO.putln("""
        Find the largest root of a quadratic function for A*x*x + B*x + C = 0.
        A cannot be 0.
        B*B - 4*A*C cannot be negative
        """);

        while(userWillContinue()){
            TextIO.putln("Please input A: ");
            A = TextIO.getDouble();

            TextIO.putln("Please input B: ");
            B = TextIO.getDouble();

            TextIO.putln("Please input C: ");
            C = TextIO.getDouble();

            try{
                largestRoot = root(A, B, C);
            } catch(IllegalArgumentException e){
                TextIO.putln(e.getMessage());
                continue;
            }

            TextIO.putln("Largest root is: " + largestRoot);
        }
    }

    public static boolean userWillContinue(){
        TextIO.putln("Find the largest quad function root? ('no' to quit, or anything else to continue)");
        String continueProgramInput = TextIO.getWord();

        return switch (continueProgramInput.toLowerCase()) {
            case "no", "n" -> false;
            default -> true;
        };
    }

    /**
     * Returns the larger of the two roots of the quadratic equation
     * A*x*x + B*x + C = 0, provided it has any roots.  If A == 0 or
     * if the discriminant, B*B - 4*A*C, is negative, then an exception
     * of type IllegalArgumentException is thrown.
     */
    static public double root( double A, double B, double C )
            throws IllegalArgumentException {
        if (A == 0) {
            throw new IllegalArgumentException("A can't be zero.");
        }
        else {
            double disc = B*B - 4*A*C;
            if (disc < 0)
                throw new IllegalArgumentException("Discriminant < zero.");
            return  (-B + Math.sqrt(disc)) / (2*A);
        }
    }
}
