import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MiniGraphingCalculator extends Application {
    public static final int CANVAS_WIDTH = 500;
    public static final int CANVAS_HEIGHT = 500;
    public static final int MIN_BOUNDARY = -5;
    public static final int MAX_BOUNDARY = 5;
    public static final int BOUNDARY_LENGTH = MAX_BOUNDARY - MIN_BOUNDARY;
    public static final int SUBDIVISION_SIZE = 300;
    private Canvas canvas;
    private GraphicsContext g;
    private TextField expressionField;
    private Text message;
    private Expr expr;
    public static void main(String[] args) {
        launch();
    }

    public void start(Stage stage) {
        // The graph will be drawn here
        canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        g = canvas.getGraphicsContext2D();

        // Default background
        clearCanvas();

        // The TextField where an expression is set
        expressionField = new TextField();

        Button enterButton = new Button("Enter");
        enterButton.setDefaultButton(true);
        enterButton.setOnAction(this::submissionHandler);

        HBox formBox = new HBox(expressionField, enterButton);
        formBox.setAlignment(Pos.CENTER);

        message = new Text();

        VBox userBox = new VBox(formBox, message) ;
        userBox.setAlignment(Pos.CENTER);
        VBox.setMargin(message, new Insets(10));
        VBox.setVgrow(message, Priority.ALWAYS);

        BorderPane pane = new BorderPane();
        pane.setCenter(canvas);
        pane.setBottom(userBox);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.setTitle("Mini Graphic Calculator");
        stage.setResizable(false);
        stage.show();
    }

    // Handles submissions of form data
    private void submissionHandler(ActionEvent e){
        try{
            message.setText("");
            expr = new Expr(expressionField.getText());
        } catch (IllegalArgumentException ex){
            //
            message.setText(ex.getMessage());
            return;
        }

        // Clear the previous graph
        g.setFill(Color.WHITE);
        clearCanvas();

        // Start graphing
        // Determine the subdivisions
        double subdivisionLength = (double) BOUNDARY_LENGTH / SUBDIVISION_SIZE;
        double y1, y2, x2, canvasX1, canvasY1, canvasX2, canvasY2;

        // Draw the graph
        for(double x1 = MIN_BOUNDARY; x1 < MAX_BOUNDARY; x1 += subdivisionLength){
            // Draw a segment
            // Determine canvas coordinates
            y1 = expr.value(x1);

            x2 = x1 + subdivisionLength;
            y2 = expr.value(x2);


            // Only draw a segment of there are y-values
            if(!(Double.isNaN(y1) || Double.isNaN(y2))){
                canvasX1 = (x1 + 5)/10 * canvas.getWidth();
                canvasY1 = (5 - y1)/10 * canvas.getHeight();

                canvasX2 = (x2 + 5)/10 * canvas.getWidth();
                canvasY2 = (5 - y2)/10 * canvas.getHeight();

                g.setStroke(Color.BLACK);
                g.strokeLine(canvasX1, canvasY1, canvasX2, canvasY2);
            }
        }
    }

    private void clearCanvas(){
        g.setFill(Color.WHITE);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
}