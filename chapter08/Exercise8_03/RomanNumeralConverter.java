import textio.TextIO;

import java.util.ArrayList;

public class RomanNumeralConverter {
    public static String userInput;
    public static void main(String[] args) {
        int number;
        int integerValue;
        String romanValue;

        while(userWantsToContinue()){
            try{
                number = Integer.parseInt(userInput);

                // The input was parsed as a number
                // See if it has the right size
                if(number <= 0 || number > 3999){
                    throw new NumberFormatException("Out of range");
                }

                // We should now have a valid integer
                romanValue = integerToRoman(number);
                integerValue = number;


            } catch (NumberFormatException e){
                // Try to treat the input as a roman numeral
                userInput = userInput.strip();

                // If the input was already determined to be invalid,
                // then simply continue
                if(e.getMessage().equals("Out of range")){
                    TextIO.putln("Input was out of range");
                    continue;
                }

                // Continue if an invalid roman numeral was provided
                if(!isValidRomanNumeral(userInput)){
                    continue;
                }

                // We should have a valid roman numeral
                integerValue = romanToInteger(userInput);
                romanValue = userInput;
            }

            TextIO.putln("Integer value: " + integerValue);
            TextIO.putln("Roman value: " + romanValue);
        }
    }

    public static int romanToInteger(String roman){
        char[] romanNumeralArray = roman.toCharArray();
        int integerValue = 0;
        int romanNumeralValue;

        for(int i = romanNumeralArray.length - 1; i >= 0; i--){
            romanNumeralValue = switch (romanNumeralArray[i]) {
                case 'M' -> 1000;
                case 'D' -> 500;
                case 'C' -> 100;
                case 'L' -> 50;
                case 'X' -> 10;
                case 'V' -> 5;
                default -> 1;
            };
            if(romanNumeralValue >= integerValue){
                integerValue += romanNumeralValue;
            } else {
                integerValue -= romanNumeralValue;
            }
        }

        return integerValue;
    }

    public static String integerToRoman(int N){
        StringBuilder romanValue = new StringBuilder();
        while(N >= 1000){
            romanValue.append("M");
            N -= 1000;
        }
        if(N >= 900){
            romanValue.append("CM");
            N -= 900;
        }
        if(N >= 500){
            romanValue.append("D");
            N -= 500;
        }
        if(N >= 400){
            romanValue.append("CD");
            N -= 400;
        }
        while(N >= 100){
            romanValue.append("C");
            N -= 100;
        }
        if(N >= 90){
            romanValue.append("XC");
            N -= 90;
        }
        while(N >= 50){
            romanValue.append("L");
            N -= 50;
        }
        if(N >= 40){
            romanValue.append("XL");
            N -= 40;
        }
        while(N >= 10){
            romanValue.append("X");
            N -= 10;
        }
        if(N >= 9){
            romanValue.append("IX");
            N -= 9;
        }
        if(N >= 5){
            romanValue.append("V");
            N -= 5;
        }
        if(N >= 4){
            romanValue.append("IV");
            N -= 4;
        }
        while(N >= 1){
            romanValue.append("I");
            N -= 1;
        }

        return romanValue.toString();
    }

    public static boolean isValidRomanNumeral(String numeral){
        char[] chArray = numeral.toCharArray();

        ArrayList<Character> chArrayList = new ArrayList<>();

        for(char ch : chArray){
            chArrayList.add(Character.toUpperCase(ch));
        }

        char[] validChars = new char[] {
          'M', 'C', 'D', 'X', 'L', 'V', 'I'
        };

        // Remove all valid roman numeral characters from character array
        char prevChar = '\0';
        int dupeCount = 1;
        for(char validChar : validChars){
            while(chArrayList.remove((Character) validChar)){
                if(prevChar == validChar){
                    dupeCount++;
                } else {
                    dupeCount = 0;
                }

                if(dupeCount >= 3){
                    return false;
                }

                prevChar = validChar;
            }
        }

        // The character array should be empty if it only had valid characters
        return chArrayList.isEmpty();

    }

    public static boolean userWantsToContinue(){
        TextIO.putln("Enter an integer between 1 and 3999, or a roman numeral in that range. Enter 'no' to quit.");

        userInput = TextIO.getln();
        switch (userInput){
            case "no", "n" -> {
                return false;
            }
            default ->{
                return true;
            }
        }
    }
}
