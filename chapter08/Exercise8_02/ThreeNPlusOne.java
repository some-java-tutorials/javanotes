import textio.TextIO;

import java.math.BigInteger;

public class ThreeNPlusOne {
    private static String userInput;
    public static void main(String[] args) {
        TextIO.putln("""
Print a 3N+1 sequence. Provide an positive integer value for N. The corresponding sequence will be displayed.""");
        BigInteger N;

        askForUserInput();

        while(!userInput.equals("")){
            try{
                N = new BigInteger(userInput);
                int sequenceCount = 1;

                if(N.signum() == -1){
                    throw new NumberFormatException("The given integer was negative");
                }

                BigInteger one = new BigInteger("1");
                BigInteger two = new BigInteger("2");

                while(!N.equals(one)){
                    sequenceCount++;
                    TextIO.putln("Sequence term #" + sequenceCount);
                    if(N.testBit(0)){
                        // N is odd: 3N + 1
                        TextIO.put(N + " is odd: 3N + 1: ");
                        N = (new BigInteger("3")).multiply(N).add(one);
                        TextIO.putln(N);
                    } else {
                        // N is even: N / 2
                        TextIO.put(N + " is even: N / 2: ");
                        N = N.divide(two);
                        TextIO.putln(N);
                    }
                    TextIO.putln();
                }
            } catch (NumberFormatException e){
                TextIO.putln(e.getMessage());
                TextIO.putln("N must be a positive integer");
            }

            // Start again by asking for input
            askForUserInput();
        }
    }

    // Asks user for user input. userInput will be set afterwards
    public static void askForUserInput(){
        TextIO.put("Provide a positive value for N: ");
        userInput = TextIO.getln();
    }
}
