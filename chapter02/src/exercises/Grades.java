package exercises;

import textio.TextIO;

/**
 * You must run this from one directory above "exercises". The file must be <code>exercises/testdata.txt</code> and must be exactly four lines with the following format:
 * <pre>
 * Student Name
 * Test Score 1
 * Test Score 2
 * Test Score 3
 * </pre>
 * Replace "Student Name" with the student's name and "Test Score #" with an integer grade.
 */
public class Grades {
    public static void main(String[] args) {
        double average;
        TextIO.readFile("exercises/testdata.txt");

        System.out.println(TextIO.getln());

        // Assume exactly three text scores for now
        average = (TextIO.getlnInt() + TextIO.getlnInt() + TextIO.getlnInt()) / 3.0;

        System.out.println("Average: " + average);
    }
}
