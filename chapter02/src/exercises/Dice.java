package exercises;

public class Dice {

    public static void main(String[] args) {
        int[] die = new int[2];
        for(int i = 0; i < die.length; i++){
            String dieNumber = i == 0 ? "first" : "second";
            die[i] = (int)(Math.random() * 6) + 1;
            System.out.println("The " + dieNumber + " die comes up " + die[i]);
        }

        System.out.println("Your total roll is " + (die[0] + die[1]));

    }
}
