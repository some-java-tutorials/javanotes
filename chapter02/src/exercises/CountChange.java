package exercises;

import textio.TextIO;

public class CountChange {
    public static void main(String[] args) {
        int numOfQuarters, numOfDimes, numOfNickels, numOfPennies;
        double totalChange;
        final float QUARTER_VALUE = .25f;
        final float DIME_VALUE = .10f;
        final float NICKEL_VALUE = .05f;
        final float PENNY_VALUE = .01f;

        System.out.println("How many quarters do you have?");
        numOfQuarters = TextIO.getlnInt();
        System.out.println("How many dimes do you have?");
        numOfDimes = TextIO.getlnInt();
        System.out.println("How many nickels do you have? ");
        numOfNickels = TextIO.getlnInt();
        System.out.println("How many pennies do you have? ");
        numOfPennies = TextIO.getlnInt();

        totalChange = numOfQuarters * QUARTER_VALUE 
            + numOfDimes * DIME_VALUE
            + numOfNickels * NICKEL_VALUE
            + numOfPennies * PENNY_VALUE;
        
        System.out.printf("Your total is $%1.2f%n", totalChange);
    }
}
