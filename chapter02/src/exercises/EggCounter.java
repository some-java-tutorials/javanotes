package exercises;

import textio.TextIO;

public class EggCounter {
    public static void main(String[] args) {
        int totalEggs, numOfDozens, numLeftOver;
        final int DOZEN = 12;

        System.out.println("How many eggs do you have?");
        totalEggs = TextIO.getlnInt();

        numOfDozens = totalEggs / DOZEN;
        numLeftOver = totalEggs % DOZEN;

        System.out.println("Your number of eggs is " + numOfDozens + " dozen, and " + numLeftOver);
    }    
}
