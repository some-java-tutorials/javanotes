package exercises;

import textio.TextIO;

public class YourName {
    public static void main(String[] args) {
        String fullName, firstName, lastName, initals;
        int numCharsFirst, numCharsLast, spaceIndex;

        System.out.println("Please enter your first name and last name, separated by a space.");

        fullName = TextIO.getln().trim();
        spaceIndex = fullName.indexOf(" ");
        firstName = fullName.substring(0, spaceIndex);
        numCharsFirst = firstName.length();
        lastName = fullName.substring(spaceIndex + 1);
        numCharsLast = lastName.length();

        System.out.println("Your first name is " + firstName + ", which has " + numCharsFirst + " characters");

        System.out.println("Your last name is " + lastName + ", which has " + numCharsLast + " characters");

        System.out.println("Your initials are " + firstName.toUpperCase().charAt(0) + lastName.toUpperCase().charAt(0));
    }
}
