package exercises;

import textio.TextIO;

public class EggCounter2 {
    public static void main(String[] args) {
        int totalEggs, numOfDozens, numGrossLeftOver, numOfGross, numLeftOver;
        final int DOZEN = 12;
        final int GROSS = 144;

        System.out.println("How many eggs do you have?");
        totalEggs = TextIO.getlnInt();

        numOfGross  = totalEggs / GROSS;
        numGrossLeftOver = totalEggs % GROSS;
        numOfDozens  = numGrossLeftOver / DOZEN;
        numLeftOver = numGrossLeftOver % DOZEN;

        System.out.println("Your number of eggs is " + numOfGross + " gross and " + numOfDozens + " dozen, and " + numLeftOver);
    }    
}
