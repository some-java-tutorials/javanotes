package exercises;

import textio.TextIO;

public class Greeting {
    public static void main(String[] args) {
        String name;

        System.out.println("Please enter your name:");

        name = TextIO.getln();

        System.out.println("Hello " + name.toUpperCase() + ", nice to meet you!");
    }
}
