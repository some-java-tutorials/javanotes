import java.util.Arrays;

public class SearchComparison {
    public final static int RANDOM_ARRAY_SIZE = 100;
    public final static int RANDOM_MAX = 500;

    public final static double ONE_BILLION = 1000000000.0;

    public static void main(String[] args) {
        // Compare selectionSort to Arrays.sort

        long startTime, endTime;
        double selectionSortTime, arraysSortTime;

        double[] randomDoubles = new double[RANDOM_ARRAY_SIZE];

        for(int i = 0; i < RANDOM_ARRAY_SIZE; i++){
            randomDoubles[i] = Math.random() * (RANDOM_MAX + 1);
        }

        System.out.println("Original list of " + RANDOM_ARRAY_SIZE + " random numbers");
        showArray(randomDoubles);
        System.out.println();

        System.out.println("Sorted list of " + RANDOM_ARRAY_SIZE + " random numbers");
        System.out.print("Selection Sort: ");

        double[] selectionSortedRandomDoubles = Arrays.copyOf(randomDoubles, randomDoubles.length);
        startTime =  System.nanoTime();
        selectionSort(selectionSortedRandomDoubles);
         endTime =  System.nanoTime();

        selectionSortTime = (endTime - startTime) / ONE_BILLION;

        System.out.println(((endTime - startTime) / ONE_BILLION) + " seconds");
        showArray(selectionSortedRandomDoubles);
        System.out.println();

        System.out.print("Arrays.sort: ");
        double[] arraysSortedRandomDoubles = Arrays.copyOf(randomDoubles, randomDoubles.length);

        startTime =  System.nanoTime();
        Arrays.sort(arraysSortedRandomDoubles);
        endTime =  System.nanoTime();

        arraysSortTime = ((endTime - startTime) / ONE_BILLION);

        System.out.println(arraysSortTime + " seconds");
        showArray(arraysSortedRandomDoubles);

        System.out.println();

        if(selectionSortTime > arraysSortTime){
            System.out.println("Arrays.sort took less time by " + (selectionSortTime - arraysSortTime) + " seconds");
        } else if (arraysSortTime > selectionSortTime) {
            System.out.println("Selection sort took less time by " + (arraysSortTime - selectionSortTime) + " seconds");
        } else {
            System.out.println("Both sorts took the same time");
        }

    }

    /**
     * <a href="https://math.hws.edu/javanotes/c7/s5.html#arrays.4.4">From Javanotes</a>
     * @param A
     */
    public static void selectionSort(double[] A) {
        // Sort A into increasing order, using selection sort

        for (int lastPlace = A.length-1; lastPlace > 0; lastPlace--) {
            // Find the largest item among A[0], A[1], ...,
            // A[lastPlace], and move it into position lastPlace
            // by swapping it with the number that is currently
            // in position lastPlace.

            int maxLoc = 0;  // Location of largest item seen so far.

            for (int j = 1; j <= lastPlace; j++) {
                if (A[j] > A[maxLoc]) {
                    // Since A[j] is bigger than the maximum we've seen
                    // so far, j is the new location of the maximum value
                    // we've seen so far.
                    maxLoc = j;
                }
            }

            double temp = A[maxLoc];  // Swap largest item with A[lastPlace].
            A[maxLoc] = A[lastPlace];
            A[lastPlace] = temp;

        }  // end of for loop

    }

    private static void showArray(double[] theArray){
        int colCounter = 1;
        for(double randomNumber : theArray){
            System.out.print(randomNumber + " ");
            colCounter++;
            // New line on every fifth column
            if(colCounter >= 5){
                colCounter = 1;
                System.out.println();
            }
        }
    }
}