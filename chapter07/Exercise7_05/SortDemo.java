import textio.TextIO;

import java.util.ArrayList;

public class SortDemo{
    private static final ArrayList<Double> numbersList = new ArrayList<>();
    private static final int MAX_SIZE = 100;

    public static void main(String[] args) {
        double input;
        Double[] sortedList;

        System.out.println("Keep entering a positive number for the list. Enter 0 to end input: ");
        while(numbersList.size() <= MAX_SIZE){
            input = -1;
            while(input < 0){
                input = TextIO.getlnDouble();
            }

            if(input == 0){
                break;
            }

            numbersList.add(input);
        }

        sortedList = insertionSort();

        for(double number : sortedList){
            System.out.println(number);
        }
    }

    /**
     * <a href="https://math.hws.edu/javanotes/c7/s5.html#arrays.4.3">From Javanotes</a>
     */
    private static Double[] insertionSort(){
        Double[] A = numbersList.toArray(new Double[] {});
        int itemsSorted; // Number of items that have been sorted so far.

        for (itemsSorted = 1; itemsSorted < A.length; itemsSorted++) {
            // Assume that items A[0], A[1], ... A[itemsSorted-1]
            // have already been sorted.  Insert A[itemsSorted]
            // into the sorted part of the list.

            double temp = A[itemsSorted];  // The item to be inserted.
            int loc = itemsSorted - 1;  // Start at end of list.

            while (loc >= 0 && A[loc] > temp) {
                A[loc + 1] = A[loc]; // Bump item from A[loc] up to loc+1.
                loc = loc - 1;       // Go on to next location.
            }

            A[loc + 1] = temp; // Put temp in last vacated space.
        }

        return A;
    }
}