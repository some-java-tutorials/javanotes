import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class DraggableShape extends Shape{
    /** If the user is dragging a shape */
    private boolean dragging;
    private double mouseCurrentX, mouseCurrentY, mousePreviousX, mousePreviousY;

    public DraggableShape(double x, double y, double w, double h, Color color){
        super(x, y, w, h, color);
    }

    public boolean isBeingDragged(){
        return dragging;
    }

    public void setDragging(boolean d){
        dragging = d;
    }

    public void setupMouseHandlers(){
        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, this::mousePressed);
        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, this::mouseDragged);
        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, this::mouseReleased);
    }

    public void mousePressed(MouseEvent evt){
        if (dragging) {
            // The user pressed a second mouse button before releasing the first.
            // Ignore the second button press.
            return;
        }

        mouseCurrentX = evt.getX();  // Remember starting position.
        mouseCurrentY = evt.getY();
        mousePreviousX = mouseCurrentX;
        mousePreviousY = mouseCurrentY;

        dragging = wantsToDrag(evt) && this.isInArea(mouseCurrentX, mouseCurrentY);
    }

    public void mouseDragged(MouseEvent evt) {
        if ( !dragging )  {
            return;
        }

        app.redraw();

        mouseCurrentX = evt.getX(); // Current position of Mouse.
        mouseCurrentY = evt.getY();

        double xDiff = mouseCurrentX - mousePreviousX;
        double yDiff = mouseCurrentY - mousePreviousY;

        x += xDiff;
        y += yDiff;
        mousePreviousX = mouseCurrentX;
        mousePreviousY = mouseCurrentY;

        draw();
    }

    /** If this shape is outside the bounds of the canvas */
    public boolean isOutOfBounds(){
        return x <= -getWidth() || x >= canvas.getWidth() || y <= -getHeight() || y > canvas.getHeight();
    }

    public void mouseReleased(MouseEvent evt) {
        if ( !dragging ){
            return;             
        }
        dragging = false;  // We are done dragging.

        if(this.isOutOfBounds()){
            app.getShapes().remove(this);
        }
    }

    /** If the user wants to drag a shape */
    public static boolean wantsToDrag(MouseEvent evt){
        return evt.isShiftDown() || evt.isSecondaryButtonDown();
    }
}
