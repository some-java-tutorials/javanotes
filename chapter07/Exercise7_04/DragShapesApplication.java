import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import java.util.ArrayList;

public class DragShapesApplication extends Application {
    private Canvas canvas;
    private GraphicsContext g;
    private ArrayList<DraggableShape> shapes;
    private final int width = 640,
     height = 480,
     shapeWidth = 50,
     shapeHeight = 50;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        canvas = new Canvas(width,height);

        Pane root = new Pane(canvas);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Left-click to create shapes, right/ctrl-click to drag shapes");
        stage.setResizable(false);

        g = canvas.getGraphicsContext2D();

        initializeStage();

        scene.setOnKeyPressed(this::keyPressed);
        canvas.setOnMousePressed(this::drawShape);

        stage.show();
    }

    public Canvas getCanvas(){
        return canvas;
    }
    public ArrayList<DraggableShape> getShapes() {
        return shapes;
    }

    private void initializeStage(){
        clear();
    }

    private void drawBackground(){
        g.setFill(Color.WHITE);
        g.fillRect(0,0,width,height);
    }

    private void drawShape(MouseEvent e){
        /*
        If the user does not want to drag a shape, it is assumed that they want to create one
         */
        if(e.isPrimaryButtonDown() && !DraggableShape.wantsToDrag(e)){
            DraggableShape shape = new DraggableShape(
                    e.getX(),
                    e.getY(),
                    shapeWidth,
                    shapeHeight,
                    Color.hsb(Math.random() * (360 + 1), 1, 1)
            );
            shape.setApplication(this);
            shape.draw();
            shape.setupMouseHandlers();
            shapes.add(shape);
        }

    }

    private void clear(){
        drawBackground();
        shapes = new ArrayList<>();
    }

    private void keyPressed(KeyEvent evt){
        if(evt.getCode() == KeyCode.ESCAPE){
            clear();
        }
    }

    public void redraw(){
        drawBackground();

        for (DraggableShape shape : shapes) {
            if (!shape.isBeingDragged()) {
                shape.draw();
            }
        }
    }
}
