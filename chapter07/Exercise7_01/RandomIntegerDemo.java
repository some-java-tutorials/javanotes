import textio.TextIO;

import java.util.ArrayList;

public class RandomIntegerDemo{
    public static void main(String[] args) {
        int listLength, maxValue;
        ArrayList<Integer> randomList;
        System.out.print("Enter the length of the random list: " );
        listLength = TextIO.getlnInt();

        System.out.print("Enter the maximum value of each random integer: ");
        maxValue = TextIO.getlnInt();

        randomList = makeRandomList(listLength, maxValue);
        for(int num : randomList){
            System.out.println(num);
        }
    }

    /**
     * Make a random list of "max" integers
     * @param listLength The length of the list
     * @param max The largest random value
     */
    private static ArrayList<Integer> makeRandomList(int listLength, int max){
        ArrayList<Integer> randomList = new ArrayList<>();
        for(int i = 0; i < listLength; i++){
            randomList.add((int) (Math.random() * (max + 1)));
        }

        return randomList;
    }
}
