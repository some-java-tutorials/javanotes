
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.input.MouseEvent;

/**
 * This panel lets two users play Go Moku against each other.
 * Black always starts the game. A player can place their piece in any empty square. When a player gets five in a row,
 * laterally or diagonally, the game ends.
 */
public class GoMoku extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
    //---------------------------------------------------------------------
    
    GoBoard board; // A canvas on which a Go board is drawn,
                         // defined by a static nested subclass.  Much of
                         // the game logic is defined in this class.


    private Button newGameButton;  // Button for starting a new game.
    
    private Button resignButton;   // Button that a player can use to end 
                                    // the game by resigning.

    private Label message;  // Label for displaying messages to the user.

    /**
     * The constructor creates the Board (which in turn creates and manages
     * the buttons and message label), adds all the components, and sets
     * the bounds of the components.  A null layout is used.  (This is
     * the only thing that is done in the main GoMoku class.)
     */
    public void start(Stage stage) {

        /* Create the label that will show messages. */
        
        message = new Label("Click \"New Game\" to begin.");
        message.setTextFill( Color.rgb(100,255,100) ); // Light green.
        message.setFont( Font.font(null, FontWeight.BOLD, 18) );
        
        /* Create the buttons and the board.  The buttons MUST be
         * created first, since they are used in the GoBoard
         * constructor! */

        newGameButton = new Button("New Game");
        resignButton = new Button("Resign");

        board = new GoBoard(); // a subclass of Canvas, defined below
        board.drawBoard();  // draws the content of the Go board
        
        /* Set up ActionEvent handlers for the buttons and a MousePressed handler
         * for the board.  The handlers call instance methods in the board object. */

        newGameButton.setOnAction( e -> board.doNewGame() );
        resignButton.setOnAction( e -> board.doResign() );
        board.setOnMousePressed( e -> board.mousePressed(e) );

        /* Set the location of each child by calling its relocate() method */

        board.relocate(20,20);
        newGameButton.relocate(370, 120);
        resignButton.relocate(370, 200);
        message.relocate(20, 370);
        
        /* Set the sizes of the buttons.  For this to have an effect, make
         * the butons "unmanaged."  If they are managed, the Pane will set
         * their sizes. */
        
        resignButton.setManaged(false);
        resignButton.resize(100,30);
        newGameButton.setManaged(false);
        newGameButton.resize(100,30);
        
        /* Create the Pane and give it a preferred size.  If the
         * preferred size were not set, the unmanaged buttons would 
         * not be included in the Pane's computed preferred size. */
        
        Pane root = new Pane();
        
        root.setPrefWidth(500);
        root.setPrefHeight(420);
        
        /* Add the child nodes to the Pane and set up the rest of the GUI */

        root.getChildren().addAll(board, newGameButton, resignButton, message);
        root.setStyle("-fx-background-color: darkgreen; "
                           + "-fx-border-color: black; -fx-border-width:3");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Go!");
        stage.show();

    } // end start()



    // --------------------  Nested Classes -------------------------------
    
    
    /**
     * A GoMove object represents a move in the game of Go.
     * It holds the row and column of the piece that is to be placed.
     * (This class makes no guarantee that the move is legal.)    
     */
    private static class GoMove {
        int toRow, toCol;      // The square the player is placing a piece upon.
        GoMove(int r, int c) {
                // Constructor.  Just set the values of the instance variables.
            toRow = r;
            toCol = c;
        }
    }  // end class GoMove.



    /**
     * This canvas displays a 320-by-320 Go board pattern with
     * a 2-pixel dark red border.  The canvas will be exactly
     * 324-by-324 pixels. This class contains methods that are
     * called in response to a mouse click on the canvas and
     * in response to clicks on the New Game and Resign buttons.
     * Note that the "New Game" and "Resign" buttons must be 
     * created before the Board constructor is called, since
     * the constructor references the buttons (in the call to doNewGame()).
     */
    private class GoBoard extends Canvas {
        private static final int CANVAS_WIDTH = 324;
        private static final int CANVAS_HEIGHT = 324;

        private static final int GRID_WIDTH = CANVAS_WIDTH / GoData.BOARD_ROWS;
        private static final int GRID_HEIGHT = CANVAS_HEIGHT / GoData.BOARD_ROWS;

        GoData board; // The data for the Go board is kept here.
                            //    This board is also responsible for generating
                            //    lists of legal moves.

        boolean gameInProgress; // Is a game currently in progress?

        /* The next three variables are valid only when the game is in progress. */

        int currentPlayer;      // Whose turn is it now?  The possible values
                                //    are GoData.BLACK and GoData.WHITE.

        int selectedRow, selectedCol;   // If the current player has selected a piece to
                                        //     move, these give the row and column
                                        //     containing that piece.  If no piece is
                                        //     yet selected, then selectedRow is -1.

        boolean madeSelection; // The user just selected which grid square to move to,
                               // but has not yet confirmed.
        boolean currentPlayerWon;

        int[][] winningSquares; // A list of x, y coordinates corresponding to the column, row of the
                                // squares that contain five connected player pieces

        /**
         * Constructor.  Creates a GoData to represent the
         * contents of the Go board, and calls doNewGame to
         * start the first game.
         */
        GoBoard() {
            super(CANVAS_WIDTH,CANVAS_HEIGHT);  // canvas is 324-by-324 pixels
            board = new GoData();
            doNewGame();
        }

        /**
         * Start a new game.  This method is called when the Board is first
         * created and when the "New Game" button is clicked.  Event handling
         * is set up in the start() method in the main class.
         */
        void doNewGame() {
            if (gameInProgress) {
                    // This should not be possible, but it doesn't hurt to check.
                message.setText("Finish the current game first!");
                return;
            }
            board.setupGame();
            currentPlayerWon = false;
            currentPlayer = GoData.BLACK;   // BLACK moves first.
            selectedRow = -1;   // BLACK has not yet selected a piece to move.
            message.setText("Black:  Make your move.");
            gameInProgress = true;
            newGameButton.setDisable(true);
            resignButton.setDisable(false);
            drawBoard();
        }

        /**
         * Current player resigns.  Game ends.  Opponent wins.  This method is
         * called when the user clicks the "Resign" button.  Event handling is
         * set up in the start() method in the main class.
         */
        void doResign() {
            if (!gameInProgress) {  // Should be impossible.
                message.setText("There is no game in progress!");
                return;
            }
            if (currentPlayer == GoData.BLACK)
                gameOver("BLACK resigns.  WHITE wins.");
            else
                gameOver("WHITE resigns.  BLACK wins.");
        }

        /**
         * The game ends.  The parameter, str, is displayed as a message
         * to the user.  The states of the buttons are adjusted so players
         * can start a new game.  This method is called when the game
         * ends at any point in this class.
         */
        void gameOver(String str) {
            message.setText(str);
            newGameButton.setDisable(false);
            resignButton.setDisable(true);
            gameInProgress = false;
        }

        boolean hasCurrentPlayerWon(GoMove move){
            int ct = 1;  // Number of pieces in a row belonging to the player.

            int r, c;    // A row and column to be examined

            int dirX, dirY; // Directions to be traversed

            int[][] directions = new int[][] {
                    {1, 0},
                    {0, 1},
                    {1, 1},
                    {-1, 1}
            };

            for (int[] directionSet : directions) {
                winningSquares = new int[5][2];

                dirX = directionSet[0];
                dirY = directionSet[1];

                r = move.toRow + dirX;  // Look at square in specified direction.
                c = move.toCol + dirY;

                int winningSquareIndex = 0;

                winningSquares[winningSquareIndex++] = new int[] { move.toCol, move.toRow };

                while (r >= 0 && r < GoData.BOARD_ROWS && c >= 0 && c < GoData.BOARD_COLS
                        && board.board[r][c] == currentPlayer) {
                    // Square is on the board, and it
                    // contains one of the player's pieces.
                    ct++;
                    winningSquares[winningSquareIndex++] = new int[] { c, r };
                    r += dirX;  // Go on to next square in this direction.
                    c += dirY;
                }

                if (ct >= 5) {
                    currentPlayerWon = true;
                    break;
                }

                r = move.toRow - dirX;  // Now, look in the opposite direction.
                c = move.toCol - dirY;
                while (r >= 0 && r < GoData.BOARD_ROWS && c >= 0 && c < GoData.BOARD_COLS
                        && board.board[r][c] == currentPlayer) {
                    ct++;
                    winningSquares[winningSquareIndex++] = new int[] { c, r };
                    r -= dirX;   // Go on to next square in this direction.
                    c -= dirY;
                }

                if (ct >= 5) {
                    currentPlayerWon = true;
                    break;
                }

                // If we got here, then the player has not yet won
                ct = 1;
            }

            return currentPlayerWon;
        }

        /**
         * This is called by mousePressed() when a player clicks on the
         * square in the specified row and col.  It has already been checked
         * that a game is, in fact, in progress.
         */
        void doClickSquare(int row, int col) {

            /* If the player clicked on a square where they can place a piece,
             mark this row and col as selected and return.  (This
             might change a previous selection.)  Reset the message, in
             case it was previously displaying an error message. */

            if(board.canMove(row, col) && !madeSelection) {
                if(selectedRow == -1 || selectedCol == -1){
                    selectedRow = row;
                    selectedCol = col;
                    madeSelection = true;
                    if (currentPlayer == GoData.BLACK)
                        message.setText("BLACK:  Click again to confirm your move.");
                    else
                        message.setText("WHITE:  Click again to confirm your move.");
                    drawBoard();
                    return;
                }
            } else if (board.canMove(row, col) && madeSelection){
                if(selectedRow != row || selectedCol != col){
                    // User has selected another square
                    selectedCol = col;
                    selectedRow = row;
                    drawBoard();
                    return;
                }
                // User is confirming a move at this square
                madeSelection = false;
                doMakeMove(new GoMove(row, col));
                return;
            }

            /* If no piece has been selected to be moved, the user must first
             select a piece.  Show an error message and return. */

            if (selectedRow < 0) {
                message.setText("Click the square you want to move to.");
                return;
            }

            drawBoard();

        }  // end doClickSquare()

        /**
         * This is called when the current player has chosen the specified
         * move.  Make the move, and then either end or continue the game
         * appropriately.
         */
        void doMakeMove(GoMove move) {

            board.makeMove(currentPlayer, move);

            /* The current player's turn is ended, so change to the other player.
             Get that player's legal moves.  If the player has no legal moves,
             then the game ends. */

            if (currentPlayer == GoData.BLACK) {
                // Check if player won

                if (hasCurrentPlayerWon(move))
                    gameOver("Black wins");
                else {
                    currentPlayer = GoData.WHITE;
                    message.setText("White:  Make your move.");
                }
            }
            else {
                if (hasCurrentPlayerWon(move))
                    gameOver("White wins");
                else {
                    currentPlayer = GoData.BLACK;
                    message.setText("Black:  Make your move.");
                }
            }

            /* Set selectedRow = -1 to record that the player has not yet selected
             a piece to move. */

            selectedRow = -1;
            selectedCol = -1;

            /* Make sure the board is redrawn in its new state. */
            drawBoard();

        }  // end doMakeMove();

        /**
         * Draw a Go board pattern in gray and lightGray with black borders. If a game is in progress, highlight the
         * square that the user intends to move to.
         */
        public void drawBoard() {
            
            GraphicsContext g = getGraphicsContext2D();
            g.setFont( Font.font(18) );

            /* Draw a two-pixel black border around the edges of the canvas. */

            g.setStroke(Color.DARKRED);
            g.setLineWidth(2);
            g.strokeRect(1, 1, CANVAS_WIDTH - 10, CANVAS_HEIGHT - 10);

            /* Draw the squares of the Go board and the pieces. */
            for (int row = 0; row < GoData.BOARD_ROWS; row++) {
                for (int col = 0; col < GoData.BOARD_COLS; col++) {
                    g.setFill(Color.LIGHTGRAY);
                    g.fillRect(2 + col * GRID_WIDTH, 2 + row * GRID_HEIGHT, GRID_WIDTH, GRID_HEIGHT);
                    g.setStroke(Color.BLACK);
                    g.strokeRect(2 + col * GRID_WIDTH, 2 + row * GRID_HEIGHT, GRID_WIDTH, GRID_HEIGHT);

                    if(madeSelection && selectedCol == col && selectedRow == row){
                        double originalLineWidth = g.getLineWidth();
                        g.setStroke(Color.CYAN);
                        g.setLineWidth(1.5);
                        g.strokeRect(2 + col * GRID_WIDTH, 2 + row * GRID_HEIGHT, GRID_WIDTH - 2, GRID_HEIGHT - 2);
                        g.setLineWidth(originalLineWidth);
                    }

                    switch (board.pieceAt(row, col)) {
                        case GoData.BLACK -> {
                            g.strokeOval(4 + col * GRID_WIDTH, 4 + row * GRID_HEIGHT, GRID_WIDTH - 4, GRID_HEIGHT - 4);
                            g.setFill(Color.BLACK);
                            g.fillOval(4 + col * GRID_WIDTH, 4 + row * GRID_HEIGHT, GRID_WIDTH - 4, GRID_HEIGHT - 4);
                        }
                        case GoData.WHITE -> {
                            g.strokeOval(4 + col * GRID_WIDTH, 4 + row * GRID_HEIGHT, GRID_WIDTH - 4, GRID_HEIGHT - 4);
                            g.setFill(Color.WHITE);
                            g.fillOval(4 + col * GRID_WIDTH, 4 + row * GRID_HEIGHT, GRID_WIDTH - 4, GRID_HEIGHT - 4);
                        }
                    }
                }
            }

            if(currentPlayerWon){
                // Highlight the squares of the winning pieces
                for(int[] coordinates : winningSquares){
                    g.setFill(Color.rgb(82, 244, 249, 0.4));
                    g.fillRect(2 + coordinates[0] * GRID_WIDTH, 2 + coordinates[1] * GRID_HEIGHT, GRID_WIDTH, GRID_HEIGHT);
                }
            }
        }  // end drawBoard()

        /**
         * Respond to a user click on the board.  If no game is in progress, show 
         * an error message.  Otherwise, find the row and column that the user 
         * clicked and call doClickSquare() to handle it.
         */
        public void mousePressed(MouseEvent evt) {
            if (!gameInProgress)
                message.setText("Click \"New Game\" to start a new game.");
            else {
                int col = (int)((evt.getX() - 2) / GRID_WIDTH);
                int row = (int)((evt.getY() - 2) / GRID_HEIGHT);
                if (col >= 0 && col < GoData.BOARD_COLS && row >= 0 && row < GoData.BOARD_ROWS)
                    doClickSquare(row,col);
            }
        }


    }  // end class Board



    /**
     * An object of this class holds data about a game of Go.
     * It knows what kind of piece is on each square of the Go board.
     * The player can place a BLACK or WHITE piece, depending on if they are the Black or
     * White player. A grid space can also be empty
     */
    private static class GoData {
        static final int BOARD_ROWS = 13;
        static final int BOARD_COLS = 13;

        /*  The following constants represent the possible contents of a square
            on the board.  The constants RED and BLACK also represent players
            in the game. */

        static final int
                    EMPTY = 0,
                    BLACK = 1,
                    WHITE = 2;

        int[][] board;  // board[r][c] is the contents of row r, column c.  

        /**
         * Constructor.  Create the board and set it up for a new game.
         */
        GoData() {
            board = new int[BOARD_ROWS][BOARD_COLS];
        }

        void setupGame(){
            for (int row = 0; row < BOARD_ROWS; row++) {
                for (int col = 0; col < BOARD_COLS; col++) {
                    board[row][col] = EMPTY;
                }
            }
        }

        /**
         * Return the contents of the square in the specified row and column.
         */
        int pieceAt(int row, int col) {
            return board[row][col];
        }

        /**
         * Make the move from (fromRow,fromCol) to (toRow,toCol).  It is
         * assumed that this move is legal.  If the move is a jump, the
         * jumped piece is removed from the board.  If a piece moves to
         * the last row on the opponent's side of the board, the 
         * piece becomes a king.
         */
        void makeMove(int player, GoMove move) {
            board[move.toRow][move.toCol] = player;
        }

        /**
         * This is called by the getLegalMoves() method to determine whether
         * the player can legally move to (r,c).
         */
        private boolean canMove(int r, int c) {

            if (r < 0 || r >= BOARD_ROWS || c < 0 || c >= BOARD_COLS)
                return false;  // (r,c) is off the board.

            return board[r][c] == EMPTY;  // (r2,c2) already contains a piece.

        }  // end canMove()

    } // end class GoData


} // end class GoMoku

