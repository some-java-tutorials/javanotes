public class TransposeDemo {
    public static void main(String[] args) {
        int[][][] testArrays = {
                {
                        {1, 2},
                        {3, 4},
                        {5, 6}
                },
                {
                        {1, 2},
                        {3, 4}
                },
                {
                        {1, 2}
                }
        };

        for(int[][] testArray : testArrays){
            showArray(transpose(testArray));
            System.out.println();
        }
    }

    /**
     * precondition: All columns in each row must be the same length
     *
     * @param original2DArray
     * @return
     */
    private static int[][] transpose(int[][] original2DArray) {
        int originalRowLength = original2DArray.length;
        int originalColumnLength = original2DArray[0].length;

        int[][] new2DArray = new int[originalColumnLength][originalRowLength];

        for (int r = 0; r < originalColumnLength; r++) {
            for (int c = 0; c < originalRowLength; c++) {
                new2DArray[r][c] = original2DArray[c][r];
            }
        }

        return new2DArray;
    }

    private static void showArray(int[][] arrayToDisplay) {
        for (int[] entryRow : arrayToDisplay) {
            for (int entry : entryRow) {
                System.out.print(entry + " ");
            }
            System.out.println();
        }


    }
}
